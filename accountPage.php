<!-- Secure session -->
<?php include_once 'utils/sec_session.php'; ?>

<?php
    sec_session_start();
    if(!empty($_SESSION['user_id']) && !empty($_SESSION['name']) && !empty($_SESSION['surname'])) {
        $user_id = $_SESSION['user_id'];
	  } else {
      header('Location: login.php');
    }
    

    include_once 'utils/db_connect.php';
     $result = mysqli_query($mysqli, "SELECT name, surname, email from users where user_id = ".$_SESSION['user_id']);
     $mysqli->close();
     	$row = $result->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <script src="jQuery/jquery-3.2.1.js"></script>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="accountPageStyle.css">
    <link rel="stylesheet" href="background.css">
  <title>Account page</title>
    <script	src="js/sha512.js"></script>
</head>
<body>

  <!-- navbar in alto-->
    <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid miaNav">
      <div class="mio-contenitore-nav">
        <a class="navbar-brand" href="index.php" >
          <img class="myTitoloNav" alt="Title" src="foto/Cesegnam.png">
        </a>
      </div>
    </div>
    </nav>


<!-- vero corpo-->

<div class="container">
  <div class="row">


<script type="text/javascript">
var nome = "<?php echo $row['name']?>";
var cognome = "<?php echo $row['surname']?>";
var email = "<?php echo $row['email']?>";
console.log(nome + cognome + email);
console.log("<?php echo $row['name']?> " + "<?php echo $row['surname']?> " + "<?php echo $row['email']?>");
</script>

      <fieldset class="my-portrait">
        <h1 class="form-title beta title--alternate scritta-bianca">Il mio Account</h1>

        <div class="my-principali-info-account my-verbose-info">
          <form id="regForm" name="registration_form" class="my-contenitore my-form-register" onsubmit="validaModifiche(this)" action="index.php">
          <label for="my-id-form-name" class="my-label-nascoste-agg scritta-bianca">Nome</label>
          <input class="form-control" value=<?php echo $row['name']?> type="text" id="my-id-form-name" name="accountName" placeholder="Nome" required />
          <label for="my-id-form-surname" class="my-label-nascoste-agg scritta-bianca">Cognome</label>
  			  <input class="form-control" value=<?php echo $row['surname']?> type="text" id="my-id-form-surname" name="accountSurname" placeholder="Cognome" required />
          <label for="my-id-form-email" class="my-label-nascoste-agg scritta-bianca">Email</label>
          <input class="form-control" value=<?php echo $row['email']?> id="my-id-form-email" type="text" name="accountEmail" placeholder="email">
          <label for="regPassword" class="my-label-nascoste-agg scritta-bianca">Password</label>
            <input class="form-control" value="" id="regPassword" name="accountPassword" type="password" placeholder="password">
          <div class="btn-modifica-contenitore">
            <input class="btn flex-item btn-style btn-modifica" type="submit" value="Modifica">
          </div>
          </form>
        </div>

      </fieldset>

  </div>
</div>

  <script>
    function validaModifiche(form) {
      var newName = document.getElementById("my-id-form-name").value;
      var newSurname = document.getElementById("my-id-form-surname").value;
      var newEmail = document.getElementById("my-id-form-email").value;
      var newPass = document.getElementById("regPassword").value;
      if(newName.trim() !== nome || newSurname.trim() !== cognome || newEmail.trim() !== email) {
        $.post("utils/userInfo.php", {
            
            name: newName,
            surname: newSurname,
            email: newEmail
        }).done(function(e) {
        	//window.location.href = 'index.php';
            setTimeout("window.location='index.php'",5000);
        	console.log("prova0");
          });
      } else if(newPass.trim() !== "") {
        $.post("utils/userInfo.php", {
            psw: hex_sha512(newPass)
        }).done(function(e) {
        	console.log("prova1");
            window.location.href = 'index.php';
          });
      }
    }
    
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</body>
</html>
