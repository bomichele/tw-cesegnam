<?php
  include_once '../utils/db_connect.php';
  include_once '../utils/sec_session.php';
  sec_session_start();
  if(empty($_SESSION['user_id'])) {
    header('Location: ../index.php');
  }
  switch ($_POST['action']) {
      case "add":
      if(empty($_POST['nomePiatto']) || empty($_POST['prezzoPiatto'])) {
        $response_array['status'] = 'error';
        $response_array['data'] = 'I campi non possono essere vuoti.';
        break;
      }
      if($stmt = $mysqli->prepare("SELECT category_id FROM categories WHERE name = ?")) {
        $stmt->bind_param("s", $_POST['categoriaPiatto']);
        $resOk = $stmt->execute();
        if(!$resOk) {
          $stmt->close();
          $mysqli->close();
          $response_array['status'] = 'error';
          $response_array['data'] = 'Error finding category id';
          break;
        } else {
          $stmt->bind_result($categoryId);
          $stmt->fetch();
          $stmt->close();
        }
      }

      if($stmt = $mysqli->prepare("INSERT INTO products (name, price, image_path, sale) VALUES (?,?,?,?)")) {
          $desc = "blah";
          $sale = 0;
        //Inserimento prodotto
        $stmt->bind_param("sdsd", $_POST['nomePiatto'], $_POST['prezzoPiatto'], $desc, $sale);
        $resOk = $stmt->execute();
        if(!$resOk) {
          $stmt->close();
          $mysqli->close();
          $response_array['status'] = 'error';
          $response_array['data'] = 'Error inserting new product.';
          break;
        }

        $result = mysqli_query($mysqli, "SELECT MAX(product_id) AS product_id FROM products");
        if($result->num_rows > 0 ) {
          $prod = $result->fetch_assoc()['product_id'];
          if($stmt = $mysqli->prepare("INSERT INTO prod_cat (category_id, product_id) VALUES (?,?)")) {
            //Link prodotto-categoria
            $stmt->bind_param("ii", $categoryId, $prod);
            $resOk = $stmt->execute();
            if(!$resOk) {
              $stmt->close();
              $mysqli->close();
              $response_array['status'] = 'error';
              $response_array['data'] = 'Error inserting product in category.';
              break;
            }
          }
        }
        $stmt->close();
        $mysqli->close();
        $response_array['status'] = 'success';
        $response_array['data'] = $_POST['nomePiatto'] . ' inserito correttamente nel database';
        break;
      }
          break;

      case "set":
      if(empty($_POST['nomePiatto']) || empty($_POST['nuovoPrezzo'])) {
        $response_array['status'] = 'error';
        $response_array['data'] = 'I campi non possono essere vuoti.';
        break;
      }
      if ($stmt = $mysqli->prepare("UPDATE `products` SET `price`= ? WHERE `name` = ?")) {
          $stmt->bind_param('ds', $_POST['nuovoPrezzo'], $_POST['nomePiatto']);
          $resOk = $stmt->execute();
          if(!$resOk) {
            $response_array['status'] = 'error';
            $response_array['data'] = 'Error updating new price.';
            break;
          } else {
            $response_array['status'] = 'success';
            $response_array['data'] = 'Prezzo modificato correttamente.';
          }
          $stmt->close();
          $mysqli->close();
      }
          break;

      case "sale":
      if($_POST['prezzoScontato'] > 0){
        if($stmt = $mysqli->prepare("UPDATE products set `sale`= ? WHERE name = ?")) {
          $stmt->bind_param("ds", $_POST['prezzoScontato'], $_POST['nomePiatto']);
          $resOk = $stmt->execute();
          if(!$resOk) {
            $stmt->close();
            $response_array['status'] = 'error';
            $response_array['data'] = 'Error inserting the new price.';
            break;
          } else {
            $response_array['status'] = 'success';
            $response_array['data'] = 'Update complete new discounted price: ' . $_POST['prezzoScontato'];
          }
          $stmt->close();
        }
      } else {
        $response_array['status'] = 'success';
        $response_array['data'] = 'Invalid price ' . $_POST['prezzoScontato'];
      }
          break;

      case "delete":
        if($stmt = $mysqli->prepare("UPDATE products set enabled= ? WHERE name = ?")) {
          $enabled = 0;
          $stmt->bind_param("is", $enabled, $_POST['nomePiatto']);
          $resOk = $stmt->execute();
          if(!$resOk) {
            $stmt->close();
            $response_array['status'] = 'error';
            $response_array['data'] = 'Error disabling the new price.';
            break;
          } else {
            $response_array['status'] = 'success';
            $response_array['data'] = 'Update complete dish disabled';
          }
          $stmt->close();
        }
          break;

      case "r_sale":
      if($stmt = $mysqli->prepare("UPDATE products set `sale`= ? WHERE name = ?")) {
        $removeSale = 0;
        $stmt->bind_param("ds", $removeSale, $_POST['nomePiatto']);
        $resOk = $stmt->execute();
        if(!$resOk) {
          $stmt->close();
          $response_array['status'] = 'error';
          $response_array['data'] = 'Error deleting the sale.';
          break;
        } else {
          $response_array['status'] = 'success';
          $response_array['data'] = 'Update complete sale deleted';
        }
        $stmt->close();
      }
          break;
  }

  echo json_encode($response_array);

  ?>
