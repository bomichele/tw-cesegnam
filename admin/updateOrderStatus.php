<?php
  include_once '../utils/db_connect.php';
  include_once '../utils/sec_session.php';
  sec_session_start();
  if(empty($_SESSION['user_id'])) {
    header('Location: ../index.php');
  }
if($result = mysqli_query($mysqli, 'UPDATE orders SET state = "' . $_POST['nuovoStato'] . '" WHERE order_id = ' . $_POST['order_id'])) {
    $response_array['status'] = 'success';
    $response_array['data'] = 'Stato ' . $_POST['nuovoStato'] . " impostato correttamente.";
    if(!strcmp($_POST['nuovoStato'],"In consegna")) {
    if($result = mysqli_query($mysqli, 'SELECT user_id from orders WHERE order_id = ' . $_POST['order_id'])) {
      $user = $result->fetch_assoc()['user_id'];
      if($result = mysqli_query($mysqli, 'SELECT * from users WHERE user_id = ' . $user)) {
        $user = array();
        $user = $result->fetch_assoc();
        $mailTO = $user['email'];
        $subject = "Cesegnam - Ordine " . $_POST['order_id'];
        $message = $user['name'] . " " . $user['surname'] . " il tuo ordine " . $_POST['order_id'] . " è stato preparato e tra poco sarà conseganto all'indirizzo da te specificato, buon pasto e grazie per aver scelto Cesegnam :)";
        $headers = "From: cesegnam@gmail.com";
        if(!mail(utf8_decode($mailTO), utf8_decode($subject), utf8_decode($message), utf8_decode($headers)."\nContent-Type: text/plain; charset=UTF-8\nContent-Transfer-Encoding: 8bit\n")) {
          die("Error sending mail");
        }
      }
    }
    }

  } else {
    $response_array['status'] = 'error';
    $response_array['data'] = 'Impossibile impostare lo stato ' . $_POST['nuovoStato'] . ' ' . mysqli_error($mysqli);
  }
  $mysqli->close();
  echo json_encode($response_array);
 ?>