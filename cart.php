<!-- Secure session -->
<?php include_once 'utils/sec_session.php'; ?>

<?php
    sec_session_start();
    if(!empty($_SESSION['user_id']) && !empty($_SESSION['name']) && !empty($_SESSION['surname'])) {
        $user_id = $_SESSION['user_id'];
	  }

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <script src="jQuery/jquery-3.2.1.js"></script>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="carrelloStyle.css">
    <link rel="stylesheet" href="background.css">
  <title>Carrello Home</title>
</head>
<body>
<script>  console.log("DEBUG SESSIONE: " + '<?php echo $_SESSION['name'] . $_SESSION['surname'] . $_SESSION['email'] . $_SESSION['admin']; ?>');</script>
<!-- navbar in alto-->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid miaNav">
      <div class="mio-contenitore-nav">
        <!--div>
          <a class="navbar-brand" href="#" >
            <img class="myOggettoNav" alt="UserPlaceholder" src="foto/UserPlaceholder.png">
          </a>
        </div-->


        <div>
          <a class="navbar-brand" href="index.php" >
            <img class="myTitoloNav" alt="Title" src="foto/Cesegnam.png">
          </a>
        </div>


      </div>
    </div>
  </nav>

<!-- presentazione negozio-->
  <div class="container my-contenitore-corpo-vero">

    <div class="my-contenitore-esterno-riepilogo">

      <h4 class="scritta-bianca">Nel tuo carrello</h4>

      <ul class="my-contenitore-riepilogo">
        <?php
          include_once 'utils/db_connect.php';
          $result = mysqli_query($mysqli, "SELECT `order_id` FROM `orders` WHERE `user_id` = " . $user_id . " AND `state` = 'selezionato'");
          $mysqli->close();
          if ($result->num_rows > 0) {
              $nProd = 0;
              $mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
              // Internal data query, no danger of SQL injection
              $result = mysqli_query($mysqli, "SELECT * FROM `orders_details` A JOIN `products` B ON A.product_id = B.product_id WHERE A.order_id = " . $result->fetch_assoc()['order_id']);
              $mysqli->close();
              if ($result->num_rows > 0) {
                          $products = array();
                          $j = 0;
                          while($row = $result->fetch_assoc()) {
                              array_push($products, $row);
                          }
                          $nProd = count($products);
                          foreach ($products as $prod) { ?>
                            <li class="my-contenitore-line-riepilogo my-<?php echo str_replace(' ', '', $prod['name']); ?>-toggle-li">
                              <!--<button class="btn btn-default my-diminuisci-quantita" onclick="decreaseTotal(<?php echo $j; ?>)">-</button>-->
                              <div class="my-specifico-alimento">
                                <div class="my-quantita-alimento-ordinate my-quantita-<?php echo str_replace(' ', '', $prod['name']); ?>"> <?php echo $prod['quantity']?></div>
                                <div> x </div>
                                <div class="my-nome-specifico-alimento"><?php echo $prod['name']; ?></div>
                              </div>
                              <div class="my-prezzo-alimento"><?php 
                              if($prod['sale'] > 0) {
                              	echo number_format((float)$prod['sale'] * $prod['quantity'], 2, '.', '') . " €";
                              } else {
                              echo number_format((float)$prod['price'] * $prod['quantity'], 2, '.', '') . " €";
                              }
                              ?> </div>
                            </li>
                            <?php
                              $j++;
                          }
                        } else { ?>
                        <script>
                        /* nel caso non c'è niente nel carrello
                        $("#main-cart form").hide();
                        $("#empty-cart").show();
                        $("#no-products").show();
                        $(".btn-no-products").show();*/
                        </script>
                        <?php   }
                      } else {?>
                        <li class="my-contenitore-line-riepilogo toggle-li">
                          <div class="my-specifico-alimento">
                            Peccato il tuo carrello è vuoto :(
                          </div>

                        </li>
                      <?php } ?>
        </ul>
      </div>

      <div class="my-info-spedizione">Spedizioni gratis a partire da 20€</div>
      <div class="vai-al-pagamento elem-bottom-nav"><a class="btn btn-default" href="restaurantList.php">Modifica ordine</a></div>

    </div>




<!--navbar in fondo-->

  <nav class="navbar navbar-default navbar-fixed-bottom">
    <div class="container-fluid miaNav">
      <div class="mio-contenitore-nav-bottom">
        <div class="numero-elementi-nel-carrello elem-bottom-nav my-inutile-cell"><p><?php if(isset($_SESSION['nArticoli']) && $_SESSION['nArticoli'] > 0){
          echo $_SESSION['nArticoli'] . ' Articoli';} ?> </p></div>

        <div class="totale-conto-carrello elem-bottom-nav my-inutile-cell"><p><?php if(isset($_SESSION['nArticoli']) && $_SESSION['nArticoli'] > 0){
          echo 'Totale: ' . $_SESSION['total'] . ' €';} ?></p></div>
          <?php if(isset($_SESSION['nArticoli']) && $_SESSION['nArticoli'] > 0){ ?>
        <div class="vai-al-pagamento elem-bottom-nav"><a id="my-fase2-conferma-id" class="btn btn-default" href="#">Conferma e paga</a></div>
      <?php } else { ?>
        <div class="elem-bottom-nav"><a class="btn btn-default" href="restaurantList.php">Torna al listino</a></div>
        <?php } ?>
      </div>
    </div>
  </nav>

  <?php
      /*
      echo $_SESSION["user_id"];
      echo $_SESSION["name"];
      echo $_SESSION["surname"];
      echo $_SESSION["admin"];
      */
          if(empty($_SESSION["user_id"]) && empty($_SESSION["name"]) && empty($_SESSION["surname"])) { ?>  <!-- No login yet -->
              <script>


                  $("#my-fase2-conferma-id").attr("href", "register.php");

                  console.log("non loggato");
              </script>
          <?php
        } else { // Login
            if ($_SESSION['admin']) { ?>
              <script>


              $("#my-fase2-cart-id").attr("style", "display: none !important;"); /*ocio può dar danno*/
              console.log("admin");
              </script> <?php
            } else { ?>
              <script>
          

              $("#my-fase2-conferma-id").attr("href", "payPage.php");

              console.log("utente");
              </script> <?php
            }
          }
      ?>

  <!-- Writes correct label inside navbar button -->


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>
