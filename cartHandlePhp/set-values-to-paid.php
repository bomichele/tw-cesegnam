<?php

    // Secure session
    include_once '../utils/sec_session.php';
    include_once '../utils/db_connect.php';

    sec_session_start();
    if(empty($_SESSION['user_id'])) {
      header('Location: ../index.php');
    }
    if(!empty($_SESSION['user_id']) && !empty($_SESSION['name']) && !empty($_SESSION['surname'])) {
        $user_id = $_SESSION['user_id'];
	  }
    $date = date("Y-m-d");

    $_SESSION['order_id'] = $_POST['order_id'];

    if ($stmt = $mysqli->prepare("UPDATE `orders` SET `delivery_address`= ?, `oraConsegna`= ?, `payment`= ?, `state`= ?, `dataPagamento`= '$date' WHERE `order_id` = ?")) {
      $stato = "Pagato";
      $stmt->bind_param('ssssi', $_POST['indirizzo'], $_POST['orario'], $_POST['metodoPagamento'], $stato, $_SESSION['order_id']);
      $stmt->execute();
      $stmt->close();
    }

    $mysqli->close();
?>
