<?php
  include_once '../utils/db_connect.php';
  include_once '../utils/sec_session.php';
  echo "Ciao";
  sec_session_start();
  if(empty($_SESSION['user_id'])) {
    header('Location: ../index.php');
  }
  $userId = $_POST['userId'];
  $_SESSION['products'] = $_POST['products'];
  $_SESSION['quantities'] = $_POST['quantities'];
  $_SESSION['total'] = $_POST['total'];
  $_SESSION['nArticoli'] = $_POST['nArticoli'];

  $products = explode(",", $_SESSION['products']);
  $quantities = explode(",", $_SESSION['quantities']);

  $prodCorrente=0;
  // Chiedo gli id dei prodotti passati
  $productId = array();

  foreach ($products as $value) {
    $productId[$prodCorrente] = mysqli_query($mysqli, "SELECT `product_id` FROM `products` WHERE `name` = '" . $value . "'");
    $productId[$prodCorrente] = $productId[$prodCorrente]->fetch_assoc()['product_id'];
    $prodCorrente++;
  }

  // Internal data query, no danger of SQL injection
  $result = mysqli_query($mysqli, "SELECT `order_id` FROM `orders` WHERE `user_id` = " . $userId . " AND `state` = 'selezionato'");



  if ($result->num_rows > 0) {
      $order_id = $result->fetch_assoc()['order_id'];
      // Elimino tutti gli order details già presenti con quell'order id.
      mysqli_query($mysqli, "DELETE FROM `orders_details` WHERE `order_id` = " . $order_id);
  } else {
      // Internal data queries, no danger of SQL injection
      $max_order_id = mysqli_query($mysqli, "SELECT `order_id` FROM `orders` ORDER BY `order_id` DESC LIMIT 1");
      $order_id = $max_order_id->fetch_assoc()['order_id'] + 1;
      mysqli_query($mysqli, "INSERT INTO `orders` (`order_id`, `state`, `user_id`) VALUES ($order_id, 'Selezionato', $userId)");
  }
  if(count($productId) == 0) {
    mysqli_query($mysqli, "DELETE FROM `orders_details` WHERE `order_id` = " . $order_id);
  } else {
    // Aggiungo i prodotti del carrello agli orders details.
    for ($i = 0; $i < count($products); $i++) {
      $prod = $productId[$i];
      $quant = $quantities[$i];
      mysqli_query($mysqli, "INSERT INTO `orders_details` (`product_id`, `order_id`, `quantity`) VALUES ($prod, $order_id, $quant)");
    }
    mysqli_query($mysqli, "UPDATE `orders` SET `nArticoli` = " . $_POST['nArticoli']. " , `totale` = " . $_POST['total'] . " WHERE `user_id` = " . $userId . " AND `state` = 'selezionato'");
  }

  $mysqli->close();

?>
