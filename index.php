<!-- Secure session -->
<?php include_once 'utils/sec_session.php'; ?>

<?php
    sec_session_start();
    if(!empty($_SESSION['user_id']) && !empty($_SESSION['name']) && !empty($_SESSION['surname'])) {
        $user_id = $_SESSION['user_id'];
	  }
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <script src="jQuery/jquery-3.2.1.js"></script>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="indexStyle.css">
  <link rel="stylesheet" href="background.css">
  <link rel="stylesheet" href="utils/style-footer.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <title>UserPage</title>
</head>
<body>


  <script> //Set cookie for banner
          $(document).ready(function() {
            $("#cookie_accept").click(function () {
                $.get("utils/cookie.php");
                $("#banner_cookie").slideToggle();
            });
          });
      </script>


<!-- navbar in alto-->
  <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid miaNav">
    <div class="mio-contenitore-nav">

      <!--div>
        <a class="navbar-brand" href="#" >
          <img class="myOggettoNav" alt="UserPlaceholder" src="foto/UserPlaceholder.png">
        </a>
      </div-->

      <div class="btn-group my-nav-portabottone">
        <button type="button" class="btn btn-default dropdown-toggle my-contenitore-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img class="myOggettoNav" alt="UserPlaceholder" src="foto/UserPlaceholder.png"> <span class="caret myOggettoNav"></span>
        </button>
        <ul class="dropdown-menu">
          <!--elementi che devono essere visibili se non sei loggato-->
          <li class="my-elemento-unlogged"><a href="register.php">Registrati</a></li>
          <li  class="divider my-elemento-unlogged"></li>
          <li class="my-elemento-unlogged"><a href="login.php">Effettua il login</a></li>

          <!--elementi che devono essere visibili se sei loggato come cliente-->
          <li class="my-elemento-client"><a href="accountPage.php">Account</a></li>
          <li  class="divider my-elemento-client"></li>
          <li class="my-elemento-client"><a href="ordiniPage.php">Ordini</a></li>
          <li class="divider my-elemento-client"></li>
          <li class="my-elemento-client"><a href="utils/logout.php">Esci</a></li>
        </ul>
      </div>


      <div>
        <a class="navbar-brand" href="index.php" >
          <img class="myTitoloNav" alt="Title" src="foto/Cesegnam.png">
        </a>
      </div>

          <!--div class="my-carrello">
            <a class="navbar-brand" href="#" >
              <img class="myOggettoNav" alt="Cart" src="foto/basicCart.png">
            </a>
          </div-->
          <div class="my-carrello my-nav-portabottone">
            <!--button type="button" class="btn btn-default" href="#"-->
            <a id="my-fase2-cart-id" class="navbar-brand" href="#" >
              <img class="myOggettoNav" alt="Cart" src="foto/basicCart.png">
            </a>
            <!--/button-->
          </div>


    </div>
  </div>
  </nav>


<!--body-->

  <div class="container mio-corpo">
    <div class="piatti-consigliati-contenitore">
      <div class="mio-contenitore-offerte">


        <div id="carousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel" data-slide-to="0" class="active"></li>
          <li data-target="#carousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

            <div class="item active">
              <div class="my-container-elemento">
                <div class="my-foto-piatto" style="background-image: url('foto/carousel1.jpg');">
                  <h3>Cesegnam</h3>
                  <h4>sapore, tradizione, gusto</h4>
                </div>

              </div>
            </div>

            <div class="item">
              <div class="my-container-elemento">
                <div class="my-foto-piatto" style="background-image: url('foto/carousel2.jpg');">
                </div>

              </div>
            </div>

        </div>   <!-- Chiusura slides wrapper -->

        <!-- Controls -->
       <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
         <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
         <span class="sr-only">Previous</span>
       </a>
       <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
         <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
       </a>

     </div>


      </div>
    </div>
  </div>

    <div class="container my-contenitore-fornitori">


      <div class="my-contenitore-suggeriti">
        <a class="my-contenitore-negozio my-stile-contenitore-negozio my-stile-sezione-fase-2" href="restaurantList.php?categoria=Pizze">
          <div class="my-foto-fornitore">
            <img src="foto/pizza.png" alt="Fornitore 1" style="max-width: 150px !important">
          </div>
          <div class="my-contenitore-info-fornitore">
            <div class="my-info-fornitore"><p>PIZZE</p></div>
          </div>
        </a>

        <a class="my-contenitore-negozio my-stile-contenitore-negozio my-stile-sezione-fase-2" href="restaurantList.php?categoria=Piade">
          <div class="my-foto-fornitore">
            <img src="foto/piadina-artigianale.png" alt="Fornitore 1" style="max-width: 128px !important">
          </div>
          <div class="my-contenitore-info-fornitore">
            <div class="my-info-fornitore"><p>PIADINE</p></div>
          </div>
        </a>

        <a class="my-contenitore-negozio my-stile-contenitore-negozio my-stile-sezione-fase-2" href="restaurantList.php?categoria=Panini">
          <div class="my-foto-fornitore">
            <img src="foto/panini.png" alt="Fornitore 1">
          </div>
          <div class="my-contenitore-info-fornitore">
            <div class="my-info-fornitore"><p>PANINI</p></div>
          </div>
        </a>

        <a class="my-contenitore-negozio my-stile-contenitore-negozio my-stile-sezione-fase-2" href="restaurantList.php?categoria=Rosticceria">
          <div class="my-foto-fornitore">
            <img src="foto/pollo.png" alt="Fornitore 1">
          </div>
          <div class="my-contenitore-info-fornitore">
            <div class="my-info-fornitore"><p>ROSTICCERIA</p></div>
          </div>
        </a>

        <a class="my-contenitore-negozio my-stile-contenitore-negozio my-stile-sezione-fase-2" href="restaurantList.php?categoria=Dolci">
          <div class="my-foto-fornitore">
            <img src="foto/dolci.png" alt="Fornitore 1" style="max-width: 114px !important">
          </div>
          <div class="my-contenitore-info-fornitore">
            <div class="my-info-fornitore"><p>DOLCI</p></div>
          </div>
        </a>

        <a class="my-contenitore-negozio my-stile-contenitore-negozio my-stile-sezione-fase-2" href="restaurantList.php?categoria=Bevande">
          <div class="my-foto-fornitore">
            <img src="foto/bibite.png" alt="Fornitore 1">
          </div>
          <div class="my-contenitore-info-fornitore">
            <div class="my-info-fornitore"><p>BEVANDE</p></div>
          </div>
        </a>
      </div>

      <a href="restaurantList.php" class="btn btn-info btn-lg btn-block my-fase2-btn-delcara">Tutte le pietanze</a>

    </div>

    <!-- Writes correct label inside navbar button -->
        <?php
        /*
        echo $_SESSION["user_id"];
        echo $_SESSION["name"];
        echo $_SESSION["surname"];
        echo $_SESSION["admin"];
        */
            if(empty($_SESSION["user_id"]) && empty($_SESSION["name"]) && empty($_SESSION["surname"])) { ?>  <!-- No login yet -->
                <script>
                    $(".my-elemento-client").attr("style", "display: none !important;");
                    $(".my-elemento-unlogged").attr("style", "display: block !important;");
                    $("#my-fase2-cart-id").attr("href", "register.php");
                    console.log("non loggato");
                </script>
            <?php
          } else { // Login
              if ($_SESSION['admin']) { ?>
                <script>
                $(".my-elemento-client").attr("style", "display: block !important;");
                $(".my-elemento-unlogged").attr("style", "display: none !important;");
                $("#my-fase2-cart-id").attr("style", "display: none !important;"); /*ocio può dar danno*/
                console.log("admin");
                </script> <?php
              } else { ?>
                <script>
                $(".my-elemento-client").attr("style", "display: block !important;");
                $(".my-elemento-unlogged").attr("style", "display: none !important;");
                $("#my-fase2-cart-id").attr("href", "cart.php");
                console.log("utente");
                </script> <?php
              }
            }
        ?>

        <?php //Banner Cookie
          $cookie_name = "coockie_utente";
          if(!isset($_COOKIE[$cookie_name])) { ?>
            <div id="banner_cookie" class="align-items-center cookie_message justify-content-center">
              <span>
                Questo sito utilizza cookie propri e cookie tecnici. Per maggiori informazioni visita
                <a href="http://www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/3585077">questa pagina</a>.
                Proseguendo con la navigazione accetterai l'utilizzo dei cookie.
              </span>
              <button id="cookie_accept">Accetta</button>
            </div> <?php
          }
        ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
	<?php include 'utils/footer.html'?>
</body>
</html>
