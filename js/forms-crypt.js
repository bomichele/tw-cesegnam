function formhash(form, password) {
   // Crea un elemento di input che verrà usato come campo di output per la password criptata.
   var p = document.createElement("input");
   // Aggiungi un nuovo elemento al tuo form.
   form.appendChild(p);
   p.id = "p";
   p.name = "p";
   p.type = "hidden"
   p.value = hex_sha512(password.value);
   // Assicurati che la password non venga inviata in chiaro.
   password.value = "";
   // Come ultimo passaggio, esegui il 'submit' del form.
   //form.submit(); oraz l'ha tolto
}

//Controlla che le password inserite nei campi "Password" e "Conferma Password" nella form siano uguali.
function validatePassword(form, password, confPassword) {
	var pass = document.getElementById("regPassword").value;
	var confirmPassword = document.getElementById("confRegPassword").value;
  console.log("password= "+pass);
  console.log("password= "+confirmPassword);


	if(pass != confirmPassword) {
		form.addEventListener('submit', errorValidate());
	} else {
		confPassword.value = "";
		formhash(form, password);
	}

};

function errorValidate() {
		event.preventDefault();
		document.getElementById("message").innerHTML = "Le password non corrispondono";
};
