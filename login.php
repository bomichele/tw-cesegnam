<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <script src="jQuery/jquery-3.2.1.js"></script>
  <?php include_once 'utils/includes.php';
  include_once 'utils/sec_session.php';
  sec_session_start();
      if(!empty($_SESSION['user_id'])) {
        header('Location: index.php');
  	  } ?>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="loginStyle.css">
    <link rel="stylesheet" href="background.css">

  <!--parte di login presa da https://www.wikihow.it/Creare-uno-Script-Sicuro-per-il-Login-Usando-PHP-e-MySQL-->
  <script type="text/javascript" src="js/sha512.js"></script>
  <script type="text/javascript" src="js/forms-crypt.js"></script>
  <!--fine parte di login-->

  <title>Login</title>
</head>



<body>

  <!--parte di login presa da https://www.wikihow.it/Creare-uno-Script-Sicuro-per-il-Login-Usando-PHP-e-MySQL-->

  <?php
  if(isset($_GET['error'])) {
     echo 'Error Logging In!';
  }
  ?>

  <!--fine parte di login-->

  <!-- navbar in alto-->
    <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid miaNav">
      <div class="mio-contenitore-nav">
        <a class="navbar-brand" href="index.php" >
          <img class="myTitoloNav" alt="Title" src="foto/Cesegnam.png">
        </a>
      </div>
    </div>
    </nav>


<!-- vero corpo-->

  <div class="my-corpo">
    <div class="container my-contenitore">
      <!--div class="my-contenitore-immagine-profilo">
        <img class="my-immagine-profilo" src="foto/tay1.jpg" alt="La tua foto del profilo">
      </div-->

      <form class="my-contenitore my-form-login" action="utils/process_login.php" method="post" name="login_form" onsubmit="formhash(this, this.password);">
        <label for="my-id-username" class="my-invisible-agg">inserisci username</label>
        <input class ="form-control" id="my-id-username" name="email" type="text" placeholder="mail">
        <label for="my-id-password" class="my-invisible-agg">inserisci password</label>
        <input class ="form-control" id="my-id-password" name="password" type="password" placeholder="password">
        <input class="btn flex-item btn-style" type="submit" value="Login" />
        <a href="register.php">or register</a>
      </form>

    </div>
  </div>

  <?php
    if(isset($_GET['error'])) {
      switch($_GET['error']) {
        case 0:
        ?>
        <script type="text/javascript">
          alert('Errore tecnico');
        </script>
        <?php
          break;
        case 1:
        ?>
        <script type="text/javascript">
          alert('Password non corretta');
        </script>
        <?php
          break;
        case 2:
        ?>
        <script type="text/javascript">
          alert('L\'utente inserito non esiste');
        </script>
        <?php
          break;

      }
    }
    if(isset($_GET['message'])) {
      switch($_GET['message']) {
        case 1:
        ?>
        <script type="text/javascript">
          $('.alert').show();
          $('.alert').html('Abbiamo inviato una mail contenente la tua password all\'indirizzo da te specificato');
        </script>
        <?php
          break;
      }
    }
  ?>




  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>
