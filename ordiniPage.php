<!-- Secure session -->
<?php include_once 'utils/sec_session.php'; ?>

<?php
    sec_session_start();
    if(!empty($_SESSION['user_id']) && !empty($_SESSION['name']) && !empty($_SESSION['surname'])) {
        $user_id = $_SESSION['user_id'];
	  } else {
      header('Location: login.php');
    }
?>

<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="UTF-8">
  <script src="jQuery/jquery-3.2.1.js"></script>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="ordiniPageStyle1.css">
    <link rel="stylesheet" href="background.css">
  <title>Pagina Ordini</title>
</head>
<body>

  <!-- navbar in alto-->
    <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid miaNav">
      <div class="mio-contenitore-nav">
        <a class="navbar-brand" href="index.php" >
          <img class="myTitoloNav" alt="Title" src="foto/Cesegnam.png">
        </a>
      </div>
    </div>
    </nav>


<!-- vero corpo-->

  <div id="my-container-id" class="container">

    <div class="my-corpo">

      <?php

        $myIdOrdini = array();
        $myState = array();
        $myOraConsegna = array();
        $myIndirizzo = array();
        $myTotale = array();

        /*nel caso sia un admin*/
        if ($_SESSION['admin']) {

          include_once 'utils/db_connect.php';

          /*query con cui tiro su tutti i prodotti*/
          $mysqli3 = new mysqli(HOST, USER, PASSWORD, DATABASE);
          // Internal data query, no danger of SQL injection
          $result3 = mysqli_query($mysqli, "SELECT * FROM `orders` WHERE NOT `state` = 'Selezionato' AND NOT `state` = 'Consegnato'");
          $mysqli3->close();

          if ($result3->num_rows > 0) {
            $myRes3Appoggio = array();
            while($row3 = $result3->fetch_assoc()) {
                array_push($myRes3Appoggio, $row3);
            }
            for ($i=0; $i<count($myRes3Appoggio) ;$i++){
              array_push($myIdOrdini, $myRes3Appoggio[$i]['order_id']);
              array_push($myState, $myRes3Appoggio[$i]['state']);
              array_push($myOraConsegna, $myRes3Appoggio[$i]['oraConsegna']);
              array_push($myIndirizzo, $myRes3Appoggio[$i]['delivery_address']);
              array_push($myTotale, $myRes3Appoggio[$i]['totale']);
            }

            ?>


            <script type="text/javascript">
            var myIdOrdini = new Array();
            var myState = new Array();
            var myOraConsegna = new Array();
            var myIndirizzo = new Array();
            var myTotale = new Array();

            <?php foreach ($myIdOrdini as $value) {
              ?>
              myIdOrdini.push(<?php echo json_encode($value); ?>)
              <?php
            } ?>

            <?php foreach ($myState as $value) {
              ?>
              myState.push(<?php echo json_encode($value); ?>)
              <?php
            } ?>

            <?php foreach ($myOraConsegna as $value) {
              ?>
              myOraConsegna.push(<?php echo json_encode($value); ?>)
              <?php
            } ?>

            <?php foreach ($myIndirizzo as $value) {
              ?>
              myIndirizzo.push(<?php echo json_encode($value); ?>)
              <?php
            } ?>

            <?php foreach ($myTotale as $value) {
              ?>
              myTotale.push(<?php echo json_encode($value); ?>)
              <?php
            } ?>




            console.log("tutti ordini: ", myIdOrdini);
            console.log("tutti i stati: ", myState);
            console.log("tutte le ore: ", myOraConsegna);
            console.log("tutti i indirizzi: ", myIndirizzo);
            console.log("tutti i totali: ", myTotale);



            </script>

              <?php

            /*quello che succede per ogni ordine tirato su*/
            for ($nO = 0; $nO < count($myIdOrdini); $nO++ ){
              $myProductName = array();
              $myPrice = array();
              $myQuantita = array();

              $mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
              // Internal data query, no danger of SQL injection
              $result = mysqli_query($mysqli, "SELECT * FROM `orders_details` A JOIN `products` B ON A.product_id = B.product_id WHERE A.order_id = " . $myIdOrdini[$nO]);
              $mysqli->close();

              if ($result->num_rows > 0) {
                $myTuttiAppoggio = array();
                while($row = $result->fetch_assoc()) {
                    array_push($myTuttiAppoggio, $row);
                }
                for ($i=0; $i<count($myTuttiAppoggio) ;$i++){
                  array_push($myProductName, $myTuttiAppoggio[$i]['name']);
                  array_push($myPrice, $myTuttiAppoggio[$i]['price']);
                  array_push($myQuantita, $myTuttiAppoggio[$i]['quantity']);
                }
              }

              ?>

              <script type="text/javascript">

                /*fare la post e cambiare le robe negli array solo se andata a buon fine*/

                function avanzaStato(numeroOrdine){
                  console.log(numeroOrdine);
                  console.log(myIdOrdini[numeroOrdine], myState[numeroOrdine], myTotale[numeroOrdine]);

                  if (myState[numeroOrdine]=="Pagato"){
                    var response = $.post("admin/updateOrderStatus.php", {
                        nuovoStato: "In consegna",
                        order_id: myIdOrdini[numeroOrdine]
                    }, "json");
                    response.success(function(data) {
                      var response = jQuery.parseJSON(data);
                      if(response.status === "success") {
                        $(".my-elemento-stato-".concat(numeroOrdine)).text("In consegna");
                      } else if(response.status === "error") {
                        alert(response.data);
                      }
                    });
                    myState[numeroOrdine] = "In consegna";
                  } else if(myState[numeroOrdine]=="In consegna"){
                    var response = $.post("admin/updateOrderStatus.php", {
                        nuovoStato: "Consegnato",
                        order_id: myIdOrdini[numeroOrdine]
                    }, "json");
                    response.success(function(data) {
                      var response = jQuery.parseJSON(data);
                      if(response.status === "success") {
                        $(".my-elemento-stato-".concat(numeroOrdine)).text("Consegnato");
                      } else if(response.status === "error") {
                        alert(response.data);
                      }
                    });
                    myState[numeroOrdine] = "Consegnato";
                    $(".my-pulsante-successivo-".concat(numeroOrdine)).attr("style", "display: none !important;"); //disattiva pulsante quando arrivato a ultimo stato

                  } else {
                    console.log("errore stato non riconosciuto");
                  }


                }

              </script>

              <div class="my-elemento-ordine my-elemento-ordine-<?php echo $nO; ?>">
                <div class="my-elemento-numero-ordine my-info-large-ordine my-info-sx scritta-bianca">Ordine n° <?php echo (($nO*1)+1); ?></div>
                <div class="my-elemento-prezzo my-info-ordine my-info-dx scritta-bianca"><?php echo $myTotale[$nO]; ?>€</div>
                <div class="my-elemento-stato my-info-ordine my-info-sx my-elemento-stato-<?php echo $nO; ?> scritta-bianca"><?php echo $myState[$nO]; ?></div>
                <div class="my-elemento-dettaglio-cliente my-info-ordine my-info-cent">
                  <a class="scritta-bianca" data-placement="bottom" data-popover-content="#id-dettaglio-venditore-<?php echo $myIdOrdini[$nO]; ?>" data-toggle="popover" data-trigger="focus" href="#" tabindex="0">dettaglio</a>
                </div>
                <div class="my-elemento-data my-info-ordine my-info-dx scritta-bianca"><?php echo $myIndirizzo[$nO]; ?></div>
              </div>


              <!--parte nel dettaglio-->

              <div class="hidden center" id="id-dettaglio-venditore-<?php echo $myIdOrdini[$nO]; ?>">
                <div class="popover-body">
                  <!--elementi di riassunto-->
                  <div class="my-contenitore-elementi-popover">
                    <div class="my-elemento-tag-ora my-info-tag-fase2">Ora:</div>
                    <div class="my-elemento-ora my-info-dato-fase2"><?php echo $myOraConsegna[$nO]; ?></div>
                  </div>

                  <div class="my-riassunto-scrollabile">

                      <hr>

              <?php

              /*succede per tutti i prodotti*/
              for ($nP = 0; $nP < count($myProductName); $nP++ ){
                ?>

                <div class="my-contenitore-scontrino-dettaglio">
                  <div ><?php echo $myQuantita[$nP]; ?>X</div>
                  <div class="my-elemento-tag-nomeCibo my-info-tag-nomeCibo"><?php echo $myProductName[$nP]; ?>:</div>
                  <div class="my-elemento-prezzoCibo my-info-prezzoCibo"><?php echo $myPrice[$nP]; ?>€</div>
                </div>

                  <hr>

                <?php
              }

              ?>


                  </div>
                  <input class="my-pulsante-successivo my-pulsante-successivo-<?php echo $nO; ?>" type="button" value="passa stato successivo" onclick="avanzaStato(<?php echo $nO; ?>)">
                </div>
              </div>

              <?php
            }





          }



      } else {
        /*nel caso sia un utente*/
          if (isset($user_id)) {
          // DB connection
          include_once 'utils/db_connect.php';

          /*query con cui tiro su tutti i prodotti*/
          $mysqli2 = new mysqli(HOST, USER, PASSWORD, DATABASE);
          // Internal data query, no danger of SQL injection
          $result2 = mysqli_query($mysqli, "SELECT * FROM `orders` WHERE `user_id` = " . $user_id );
          $mysqli2->close();

          if ($result2->num_rows > 0) {
            $myRes2Appoggio = array();
            while($row2 = $result2->fetch_assoc()) {
                array_push($myRes2Appoggio, $row2);
            }
            for ($i=0; $i<count($myRes2Appoggio) ;$i++){
              array_push($myIdOrdini, $myRes2Appoggio[$i]['order_id']);
              array_push($myState, $myRes2Appoggio[$i]['state']);
              array_push($myOraConsegna, $myRes2Appoggio[$i]['oraConsegna']);
              array_push($myIndirizzo, $myRes2Appoggio[$i]['delivery_address']);
              array_push($myTotale, $myRes2Appoggio[$i]['totale']);
            }


            /*quello che succede per ogni ordine tirato su*/
            for ($nO = 0; $nO < count($myIdOrdini); $nO++ ){
              $myProductName = array();
              $myPrice = array();
              $myQuantita = array();

              $mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
              // Internal data query, no danger of SQL injection
              $result = mysqli_query($mysqli, "SELECT * FROM `orders_details` A JOIN `products` B ON A.product_id = B.product_id WHERE A.order_id = " . $myIdOrdini[$nO]);
              $mysqli->close();

              if ($result->num_rows > 0) {
                $myTuttiAppoggio = array();
                while($row = $result->fetch_assoc()) {
                    array_push($myTuttiAppoggio, $row);
                }
                for ($i=0; $i<count($myTuttiAppoggio) ;$i++){
                  array_push($myProductName, $myTuttiAppoggio[$i]['name']);
                  array_push($myPrice, $myTuttiAppoggio[$i]['price']);
                  array_push($myQuantita, $myTuttiAppoggio[$i]['quantity']);
                }
              }

              ?>

              <div class="my-elemento-ordine">
                <div class="my-elemento-numero-ordine my-info-large-ordine my-info-sx scritta-bianca">Ordine n° <?php echo (($nO*1)+1); ?></div>
                <div class="my-elemento-prezzo my-info-ordine my-info-dx scritta-bianca"><?php echo $myTotale[$nO]; ?>€</div>
                <div class="my-elemento-stato my-info-ordine my-info-sx scritta-bianca"><?php echo json_encode($myState[$nO]); ?></div>
                <div class="my-elemento-dettaglio-cliente my-info-ordine my-info-cent">
                  <a class="scritta-bianca" data-placement="bottom" data-popover-content="#id-dettaglio-cliente-<?php echo $myIdOrdini[$nO]; ?>" data-toggle="popover" data-trigger="focus" href="#" tabindex="0">dettaglio</a>
                </div>
                <div class="my-elemento-data my-info-ordine my-info-dx scritta-bianca"><?php echo $myIndirizzo[$nO]; ?></div>
              </div>


              <!--parte nel dettaglio-->

              <div class="hidden center" id="id-dettaglio-cliente-<?php echo $myIdOrdini[$nO]; ?>">
                <div class="popover-body">
                  <!--elementi di riassunto-->
                  <div class="my-contenitore-elementi-popover">
                    <div class="my-elemento-tag-ora my-info-tag-fase2">Ora:</div>
                    <div class="my-elemento-ora my-info-dato-fase2"><?php echo $myOraConsegna[$nO]; ?></div>
                  </div>

                  <div class="my-riassunto-scrollabile">

                      <hr>

              <?php

              /*succede per tutti i prodotti*/
              for ($nP = 0; $nP < count($myProductName); $nP++ ){
                ?>

                <div class="my-contenitore-scontrino-dettaglio">
                  <div ><?php echo $myQuantita[$nP]; ?>X</div>
                  <div class="my-elemento-tag-nomeCibo my-info-tag-nomeCibo"><?php echo $myProductName[$nP]; ?>:</div>
                  <div class="my-elemento-prezzoCibo my-info-prezzoCibo"><?php echo $myPrice[$nP]; ?>€</div>
                </div>

                  <hr>

                <?php
              }

              ?>

                  </div>
                </div>
              </div>

              <?php
            }
          }
        }
      }

       ?>



    </div>

  </div>




  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

  <script>



    $(function(){

        $("[data-toggle=popover]").popover({
            html : true,
            content: function() {
              var content = $(this).attr("data-popover-content");
              return $(content).children(".popover-body").html();
            },

            title: function() {
              var title = $(this).attr("data-popover-content");
              return $(title).children(".popover-heading").html();
            }

        });


    });

  </script>


</body>
</html>
