<!-- Secure session -->
<?php include_once 'utils/sec_session.php'; ?>

<?php
    sec_session_start();
    if(!empty($_SESSION['user_id']) && !empty($_SESSION['name']) && !empty($_SESSION['surname'])) {
        $user_id = $_SESSION['user_id'];
	  }
    if(!isset($_SESSION['nArticoli']) || $_SESSION['nArticoli'] <= 0) {
      header('Location: restaurantList.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <script src="jQuery/jquery-3.2.1.js"></script>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="paginaPagamentoStyle.css">
    <link rel="stylesheet" href="background.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <title>Carrello Pagamento</title>
</head>
<body>


  <script type="text/javascript">
  var orario = "";
  var indirizzo = "";
  var metodoPagamento = "";
  var usaOrario = false;
  var usaIndirizzo = false;
  </script>

<!-- navbar in alto-->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid miaNav">
      <div class="mio-contenitore-nav">
        <div class="btn-group my-nav-portabottone">
          <button type="button" class="btn btn-default dropdown-toggle my-contenitore-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="myOggettoNav" alt="UserPlaceholder" src="foto/UserPlaceholder.png"> <span class="caret myOggettoNav"></span>
          </button>
          <ul class="dropdown-menu">
            <!--elementi che devono essere visibili se non sei loggato-->
            <li class="my-elemento-unlogged"><a href="register.php">Registrati</a></li>
            <li  class="divider my-elemento-unlogged"></li>
            <li class="my-elemento-unlogged"><a href="login.php">Effettua il login</a></li>

            <!--elementi che devono essere visibili se sei loggato come cliente-->
            <li class="my-elemento-client"><a href="accountPage.php">Account</a></li>
            <li class="divider my-elemento-client"></li>
            <li class="my-elemento-client"><a href="ordiniPage.php">Ordini</a></li>
            <li class="divider my-elemento-client"></li>
            <li class="my-elemento-client"><a href="utils/logout.php">Esci</a></li>
          </ul>
        </div>

        <div class="grande-fdp">
          <a class="navbar-brand" href="index.php" >
            <img class="myTitoloNav" alt="Title" src="foto/Cesegnam.png">
          </a>
        </div>

        <div class="my-contenitore-casa-fase2">
          <a class="navbar-brand my-fase2-to-index" href="index.php" >
            <img class="myOggettoNav" alt="Homebasic" src="foto/basicHome.png">
          </a>
        </div>

        <div class="my-carrello">
          <a id="my-fase2-cart-id" class="navbar-brand" href="cart.php" >
            <img class="myOggettoNav" alt="Cart" src="foto/basicCart.png">
          </a>
        </div>
      </div>
    </div>
  </nav>

<!-- presentazione negozio-->
  <div class="container my-contenitore-corpo-vero">

    <div class="my-contenitore-esterno-riepilogo">



      <h3 class="my-presentazione-carrello scritta-bianca">Nel tuo carrello</h3>

      <ul class="my-contenitore-riepilogo">


        <?php
          include_once 'utils/db_connect.php';
          $result = mysqli_query($mysqli, "SELECT * FROM `orders` WHERE `user_id` = " . $user_id . " AND `state` = 'selezionato'");
          $mysqli->close();
          if ($result->num_rows > 0) {
              $nProd = 0;
              $row = $result->fetch_assoc();
              $totale = $row['totale'];
              $nArticoli = $row['nArticoli'];
              $orderid = $row['order_id'];
              $mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);

              // Internal data query, no danger of SQL injection
              $result = mysqli_query($mysqli, "SELECT * FROM `orders_details` A JOIN `products` B ON A.product_id = B.product_id WHERE A.order_id = " . $row['order_id']);
              $mysqli->close();
              if ($result->num_rows > 0) {
                          $products = array();
                          $j = 0;
                          while($row = $result->fetch_assoc()) {
                              array_push($products, $row);
                          }
                          $nProd = count($products);
                          foreach ($products as $prod) { ?>
                            <li class="my-contenitore-line-riepilogo my-<?php echo str_replace(' ', '', $prod['name']); ?>-toggle-li">
                              <!--<button class="btn btn-default my-diminuisci-quantita" onclick="decreaseTotal(<?php echo $j; ?>)">-</button>-->
                              <div class="my-specifico-alimento">
                                <div class="my-quantita-alimento-ordinate my-quantita-<?php echo str_replace(' ', '', $prod['name']); ?>"> <?php echo $prod['quantity']?></div>
                                <div> x </div>
                                <div class="my-nome-specifico-alimento"><?php echo $prod['name']; ?></div>
                              </div>
                              <div class="my-prezzo-alimento"><?php 
                              if($prod['sale'] > 0) {
                              	echo number_format((float)$prod['sale'] * $prod['quantity'], 2, '.', '') . " €";
                              } else {
                              	echo number_format((float)$prod['price'] * $prod['quantity'], 2, '.', '') . " €";
                              }
                              ?></div>
                            </li>
                            <?php
                              $j++;
                          }
                        } else { ?>
                        <script>
                        /* nel caso non c'è niente nel carrello
                        $("#main-cart form").hide();
                        $("#empty-cart").show();
                        $("#no-products").show();
                        $(".btn-no-products").show();*/
                        </script>
                        <?php   }
                      } ?>



      </ul>

      <div class="my-info-last-revisione">
        <div class="my-info-spedizione scritta-bianca">Spedizioni gratis a partire da 20€</div>
        <div class="my-info-totale scritta-bianca">Totale <?php echo number_format((float)$totale, 2, '.', '');?>€</div>
      </div>

    </div> <!--contenitore esterno riepilogo-->

    <div class="my-contenitore-esterno-dettagli-spedizione">
      <div class="my-contenitore-specifica-indirizzo  my-elemento-dettaglio">
        <form class="my-form-indirizzo my-form-specifico" action="/#">
          <fieldset>
            <legend class="scritta-bianca">Indirizzo</legend>
              <ul class="my-lista-agg">
                <li><input id="my-id-consegna-campus" type="radio" name="location" value="default" checked onclick="hideAltrove()"><label for="my-id-consegna-campus" class="scritta-bianca">Consegna al campus</label></li>
                <li><input id="my-id-consegna-altrove" type="radio" name="location" value="altrove" onclick="showAltrove()"><label for="my-id-consegna-altrove" class="scritta-bianca">Specifica indirizzo</label></li>
                <li><input id="my-id-consegna-altrove-testo" class="my-text-form-agg" type="text" name="indirizzo-preciso" placeholder="specifica luogo"><label for="my-id-consegna-altrove-testo" class="my-non-mostrare-agg">inserire indirizzo</label></li>
              </ul>
          </fieldset>
        </form>
      </div>

      <div class="my-contenitore-specifica-ora my-elemento-dettaglio">
        <form class="my-form-orario my-form-specifico" action="/#">
          <fieldset>
            <legend class="scritta-bianca">Orario di consegna</legend>
            <ul class="my-lista-agg">
              <li><input id="my-id-tempo-nunc" type="radio" name="orario" value="nunc" checked onclick="hideOrario()"> <label for="my-id-tempo-nunc" class="scritta-bianca">Il prima possibile</label> </li>
              <li><input id="my-id-tempo-scelto" type="radio" name="orario" value="a-scelta" onclick="showOrario()"><label for="my-id-tempo-scelto" class="scritta-bianca">Per le ore:</label></li>
              <li><input id="my-id-tempo-scelto-testo" class="my-text-form-agg" type="time" name="orario-preciso" ><label for="my-id-tempo-scelto-testo" class="my-non-mostrare-agg">inserire orario</label></li>
            </ul>
          </fieldset>
        </form>
      </div>

      <div class="my-contenitore-specifica-pagamento my-elemento-dettaglio">
        <form class="my-form-orario my-form-specifico" action="/#">
          <fieldset>
            <legend class="scritta-bianca">Paga con</legend>
            <ul class="my-lista-agg">
              <li><input id="my-id-pagamento-paypal" type="radio" name="valuta" value="Paypal" onclick="setPagamentoPaypal()" ><label for="my-id-pagamento-paypal" class="scritta-bianca">Paypal</label></li>
              <li><input id="my-id-pagamento-contanti" type="radio" name="valuta" value="Contanti" onclick="setPagamentoContanti()" checked><label for="my-id-pagamento-contanti" class="scritta-bianca">Contanti</label></li>
              <li><input id="my-id-pagamento-creditCard" type="radio" name="valuta" value="Carta" onclick="setPagamentoCarta()"><label for="my-id-pagamento-creditCard" class="scritta-bianca">Carta di credito</label></li>
            </ul>
          </fieldset>
        </form>
      </div>
    </div>

  </div>

<!--navbar in fondo-->

  <nav class="navbar navbar-default navbar-fixed-bottom">
    <div class="container-fluid miaNav">
      <div class="mio-contenitore-nav-bottom">
        <div class="numero-elementi-nel-carrello elem-bottom-nav my-inutile-cell"><p><?php echo $nArticoli?> articoli</p></div>

        <div class="totale-conto-carrello elem-bottom-nav my-inutile-cell"><p>Totale <?php echo number_format((float)$totale, 2, '.', '');?>€</p></div>

        <div class="vai-al-pagamento elem-bottom-nav"><a id="my-fase2-conferma-id" class="btn btn-default" href="#" >Conferma e paga</a></div>
      </div>
    </div>
  </nav>

  <!-- Writes correct label inside navbar button -->
      <?php
      /*
      echo $_SESSION["user_id"];
      echo $_SESSION["name"];
      echo $_SESSION["surname"];
      echo $_SESSION["admin"];
      */
          if(empty($_SESSION["user_id"]) && empty($_SESSION["name"]) && empty($_SESSION["surname"])) { ?>  <!-- No login yet -->
              <script>
                  $(".my-elemento-client").attr("style", "display: none !important;");
                  $(".my-elemento-unlogged").attr("style", "display: block !important;");
                  $("#my-fase2-conferma-id").attr("href", "register.php");
                  $("#my-fase2-cart-id").attr("style", "display: none !important;");
                  console.log("non loggato");
              </script>
          <?php
        } else { // Login
            if ($_SESSION['admin']) { ?>
              <script>
              $(".my-elemento-client").attr("style", "display: block !important;");
              $(".my-elemento-unlogged").attr("style", "display: none !important;");
              $("#my-fase2-cart-id").attr("style", "display: none !important;"); /*ocio può dar danno*/
              console.log("admin");
              </script> <?php
            } else { ?>
              <script>
              $(".my-elemento-client").attr("style", "display: block !important;");
              $(".my-elemento-unlogged").attr("style", "display: none !important;");

              /*$("#my-fase2-conferma-id").attr("href", "successfulOrder.php");*/
              console.log("utente");
              </script> <?php
            }
          }
      ?>

  <script type="text/javascript">

    function setLuogo(){
      indirizzo = "Campus";
    }

    function setAsap(){
      orario = "Il prima possibile";
    }

    function setPagamentoPaypal(){
      metodoPagamento = "Paypal";
    }

    function setPagamentoCarta(){
      metodoPagamento = "Carta";
    }

    function setPagamentoContanti(){
      metodoPagamento = "Contanti";
    }

    function showOrario(){
      $("#my-id-tempo-scelto-testo").show();
      usaOrario = true;
    }

    function hideOrario(){
      $("#my-id-tempo-scelto-testo").hide();
      usaOrario = false;
      setAsap();
    }


    function showAltrove(){
      $("#my-id-consegna-altrove-testo").show();
      usaIndirizzo = true;
    }

    function hideAltrove(){
      $("#my-id-consegna-altrove-testo").hide();
      usaIndirizzo = false;
      setLuogo();
    }

    function getIndirizzoFromForm(){
      indirizzo = $("#my-id-consegna-altrove-testo").val();
      console.log(indirizzo);
    }

    function getOrarioFromForm(){
      orario = $("#my-id-tempo-scelto-testo").val();
      console.log(orario);
    }





    $(document).ready(function() {
      hideOrario();
      hideAltrove();
      setPagamentoContanti();
    });



  </script>


  <script type="text/javascript">



  $(document).ready(function() {
    $("#my-fase2-conferma-id").click(function() {

        var orderId = <?php echo $orderid;  ?>;
        if (usaOrario == true){
          getOrarioFromForm();
        }

        if (usaIndirizzo == true){
          getIndirizzoFromForm();
        }

        console.log("invio: ", orderId, " ", indirizzo, " ", orario, " ", metodoPagamento );

        $.post("cartHandlePhp/set-values-to-paid.php", {
            order_id:  <?php echo $orderid;  ?>,
            indirizzo: indirizzo,
            orario: orario,
            metodoPagamento: metodoPagamento
        }).done(function(e) {
            window.location.href = 'successfulOrder.php';
          });

      });




    });

  </script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>
