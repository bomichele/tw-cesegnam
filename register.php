<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <script src="jQuery/jquery-3.2.1.js"></script>
  <?php include_once 'utils/includes.php';
  include_once 'utils/sec_session.php';
  sec_session_start();
      if(!empty($_SESSION['user_id'])) {
        header('Location: index.php');
  	  } ?>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="registerStyle.css">
    <link rel="stylesheet" href="background.css">
  <title>Register</title>
  <script	src="js/sha512.js"></script>
  <script src="js/forms-crypt.js"></script>
</head>
<body>

  <!-- navbar in alto-->
    <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid miaNav">
      <div class="mio-contenitore-nav">
        <a class="navbar-brand" href="index.php" >
          <img class="myTitoloNav" alt="Title" src="foto/Cesegnam.png">
        </a>
      </div>
    </div>
    </nav>


<!-- vero corpo-->

  <div class="my-corpo">
    <div class="container my-contenitore">

      <form id="regForm" name="registration_form" class="my-contenitore my-form-register"  action="utils/register_user.php" method="post" onsubmit="validatePassword(this, this.regPassword, this.confRegPassword);">
        <label for="my-id-form-name" class="my-label-nascoste-agg">Nome</label>
			  <input class="form-control" type="text" id="my-id-form-name" name="regName" placeholder="Nome" required />
        <label for="my-id-form-surname" class="my-label-nascoste-agg">Cognome</label>
			  <input class="form-control" type="text" id="my-id-form-surname" name="regSurname" placeholder="Cognome" required />
        <label for="my-id-form-email" class="my-label-nascoste-agg">inserisci email</label>
        <input class="form-control" id="my-id-form-email" type="text" name="regEmail" placeholder="email">
        <label for="regPassword" class="my-label-nascoste-agg">inserisci password</label>
        <input class="form-control" id="regPassword" name="regPassword" type="password" placeholder="password">
        <label for="confRegPassword" class="my-label-nascoste-agg">conferma password</label>
        <input class="form-control" id="confRegPassword" name="confRegPassword" type="password" placeholder="conferma password">
        <input class="btn flex-item btn-style" type="submit" value="Registrati">
      </form>

    </div>
  </div>

  <?php
		if(isset($_GET['error'])) {
			switch($_GET['error']) {
				case -1:
				?>
				<script type="text/javascript">
					$('.alert').show();
					$('.alert').html('L\'account è stato disabilitato, riprova tra 2 ore');
				</script>
				<?php
					break;
				case 0:
				?>
				<script type="text/javascript">
					$('.alert').show();
					$('.alert').html('Errore tecnico'); /*l'errore sta nel codice di invio alla pagina php*/
				</script>
				<?php
					break;
				case 1:
				?>
				<script type="text/javascript">
					$('.alert').show();
					$('.alert').html('Password non corretta');
				</script>
				<?php
					break;
				case 2:
				?>
				<script type="text/javascript">
					$('.alert').show();
					$('.alert').html('L\'utente inserito non esiste');
				</script>
				<?php
					break;
				case 3:
				?>
				<script type="text/javascript">
					$('.alert').show();
					$('.alert').html('Errore durante l\'invio della mail di recupero password');
				</script>
				<?php
					break;
			}
		}
		if(isset($_GET['message'])) {
			switch($_GET['message']) {
				case 1:
				?>
				<script type="text/javascript">
					$('.alert').show();
					$('.alert').html('Abbiamo inviato una mail contenente la tua password all\'indirizzo da te specificato');
				</script>
				<?php
					break;
			}
		}
	?>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>
