<!-- Secure session -->
<?php include_once 'utils/sec_session.php'; ?>

<?php
    sec_session_start();
    if(!empty($_SESSION['user_id']) && !empty($_SESSION['name']) && !empty($_SESSION['surname'])) {
        $user_id = $_SESSION['user_id'];
	  }
    //echo $_GET['categoria'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <script src="jQuery/jquery-3.2.1.js"></script>
  <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="background.css">
  <link rel="stylesheet" href="restaurantListStyle1.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <title>Listino Ristorante User</title>
</head>
<body>
<script>  console.log("DEBUG SESSIONE: " + '<?php echo $_SESSION['name'] . $_SESSION['surname'] . $_SESSION['email'] . $_SESSION['admin']; ?>');</script>
  <div class="background-image">

<!-- navbar in alto-->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid miaNav">
      <div class="mio-contenitore-nav">
        <div class="btn-group my-nav-portabottone">
          <button type="button" class="btn btn-default dropdown-toggle my-contenitore-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="myOggettoNav" alt="UserPlaceholder" src="foto/UserPlaceholder.png"> <span class="caret myOggettoNav"></span>
          </button>
          <ul class="dropdown-menu">
            <!--elementi che devono essere visibili se non sei loggato-->
            <li class="my-elemento-unlogged"><a href="register.php">Registrati</a></li>
            <li class="divider my-elemento-unlogged"></li>
            <li class="my-elemento-unlogged"><a href="login.php">Effettua il login</a></li>

            <!--elementi che devono essere visibili se sei loggato come cliente-->
            <li class="my-elemento-client"><a class="my-fase2-to-accountPage" href="#">Account</a></li>
            <li class="divider my-elemento-client"></li>
            <li class="my-elemento-client my-elemento-admin"><a class="my-fase2-to-ordiniPage" href="#">Ordini</a></li>
            <li class="divider my-elemento-client my-elemento-admin"></li>
            <li class="my-elemento-client my-elemento-admin "><a class="my-fase2-to-logout" href="#">Esci</a></li>
          </ul>
        </div>

        <div class="my-contenitore-titolo">
          <a class="navbar-brand my-fase2-to-index" href="#" >
            <img class="myTitoloNav" alt="Title" src="foto/Cesegnam.png">
          </a>
        </div>

        <div class="my-contenitore-casa-fase2">
          <a class="navbar-brand my-fase2-to-index" href="#" >
            <img class="myOggettoNav" alt="Home" src="foto/basicHome.png">
          </a>
        </div>

        <div class="my-carrello">
          <a id="my-fase2-cart-id" class="my-fase-2-btn-to-cart" href="#" >
            <img class="myOggettoNav" alt="Cart" src="foto/basicCart.png">
          </a>
        </div>
      </div>
    </div>
  </nav>

<!-- presentazione negozio-->
  <div class="container">
    <div class="row">

      <div class="my-contenitore-centrale-fase-2">

        <div class="my-sidebar-aside-container">



        <?php
        if(isset($_SESSION['admin']) && $_SESSION['admin'] == 0) {
        ?>
        <aside class="my-riepilogo" id="my-riepilogo-fase-2">
          <h4>Riepilogo:</h4>
          <hr class="my-fase2-divisore-boh">

          <ul class="my-contenitore-riepilogo-scrollable-fase-2 ">

            <?php
            $total = 0.00;
            $prices = array();
            $quantities = array();

            /*inizializzo i 3 vettori in cui metterò tutti i prodotti, le quantita e i prezzi*/
            $myTuttiProdottiPhp = array();
            $myTutteQuantitaPhp = array();
            $myTuttiPrezziPhp = array();

            /*inizializzo i 3 vettori in cui metterò solo i prodotti ordinati, le quantita e i prezzi*/
            $myOrdinatiProdottiPhp = array();
            $myOrdinateQuantitaPhp = array();
            $myOrdinatiPrezziPhp = array();

            $myTuttiScontiPhp = array();

            ?>
            <script type="text/javascript">


            /*inizializzo i 3 vettori in cui metterò tutti i prodotti, le quantita e i prezzi JS*/
            var myTuttiProdottiJS = new Array();
            var myTutteQuantitaJS = new Array();
            var myTuttiPrezziJS = new Array();

            /*inizializzo i 3 vettori in cui metterò solo i prodotti ordinati, le quantita e i prezzi JS*/
            var myOrdinatiProdottiJS = new Array();
            var myOrdinateQuantitaJS = new Array();
            var myOrdinatiPrezziJS = new Array();

            /*inizializzo i 3 vettori in cui metterò solo i prodotti, le quantita e i prezzi che dovranno essere memorizzati JS*/
            var myToPostProdottiJS = new Array();
            var myToPostQuantitaJS = new Array();
            var myToPostPrezziJS = new Array();

            var myTuttiScontiJS = new Array();

            </script>
            <?php

            /*non cancellare gli script di stampa che i vettori che usi li crei li e non cancelare nemmeno sta variabile*/
            ?>
            <script type="text/javascript">
              var rendereInvisibile = false;
            </script>
            <?php

            $j = 0;
            if (isset($user_id)) {
                // DB connection
                include_once 'utils/db_connect.php';

                /*query con cui tiro su tutti i prodotti*/
                $mysqli2 = new mysqli(HOST, USER, PASSWORD, DATABASE);
                // Internal data query, no danger of SQL injection
                $result2 = mysqli_query($mysqli, "SELECT * FROM `products`");
                $mysqli2->close();

                if ($result2->num_rows > 0) {
                  $myTuttiAppoggio = array();
                  while($row = $result2->fetch_assoc()) {
                      array_push($myTuttiAppoggio, $row);
                  }
                  for ($i=0; $i<count($myTuttiAppoggio) ;$i++){
                    array_push($myTuttiProdottiPhp, $myTuttiAppoggio[$i]['name']);
                    array_push($myTuttiPrezziPhp, $myTuttiAppoggio[$i]['price']);
                    array_push($myTutteQuantitaPhp, "0");
                    array_push($myTuttiScontiPhp, $myTuttiAppoggio[$i]['sale']);
                  }
                }

                ?>

                <script type="text/javascript">

                  <?php foreach ($myTuttiProdottiPhp as $value) {
                    ?>
                    myTuttiProdottiJS.push(<?php echo json_encode($value); ?>)
                    <?php
                  } ?>


                  <?php foreach ($myTuttiPrezziPhp as $value) {
                    ?>
                    myTuttiPrezziJS.push(<?php echo json_encode($value); ?>)
                    <?php
                  } ?>


                  <?php foreach ($myTutteQuantitaPhp as $value) {
                    ?>
                    myTutteQuantitaJS.push(<?php echo json_encode($value); ?>)
                    <?php
                  } ?>

                  <?php foreach ($myTuttiScontiPhp as $value) {
                    ?>
                    myTuttiScontiJS.push(<?php echo json_encode($value); ?>)
                    <?php
                  } ?>

                  console.log("tutti i sconti: ", myTuttiScontiJS);
                  console.log("tutti i prodotti: ", myTuttiProdottiJS);
                  console.log("tutti i prezzi: ", myTuttiPrezziJS);
                  console.log("tutte le quantità: ", myTutteQuantitaJS);
                </script>

                <?php


                $result = mysqli_query($mysqli, "SELECT `order_id` FROM `orders` WHERE `user_id` = " . $user_id . " AND `state` = 'Selezionato'");
                $mysqli->close();

                /*qua va fatto qualcosa se è tutto vuoto*/

                if ($result->num_rows > 0) {
                    $mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
                    // Internal data query, no danger of SQL injection
                    $result = mysqli_query($mysqli, "SELECT * FROM `orders_details` A JOIN `products` B ON A.product_id = B.product_id WHERE A.order_id = " . $result->fetch_assoc()['order_id']);
                    $mysqli->close();

                    /*inizio parte nuova*/

                    if ($result->num_rows > 0) {
                          $myOrdinatiAppoggio = array();
                          while($row = $result->fetch_assoc()) {
                              array_push($myOrdinatiAppoggio, $row);
                          }
                          for ($i=0; $i<count($myOrdinatiAppoggio) ;$i++){
                            array_push($myOrdinatiProdottiPhp, $myOrdinatiAppoggio[$i]['name']);
                            array_push($myOrdinateQuantitaPhp, $myOrdinatiAppoggio[$i]['quantity']);
                            if($myOrdinatiAppoggio[$i]['sale'] > 0) {
                              array_push($myOrdinatiPrezziPhp, $myOrdinatiAppoggio[$i]['sale']);
                            } else {
                              array_push($myOrdinatiPrezziPhp, $myOrdinatiAppoggio[$i]['price']);
                            }
                          }

                          ?>

                          <script type="text/javascript">
                            var myOrdinatiAppoggioJS = new Array();
                            <?php foreach ($myOrdinatiAppoggio as $value) {
                              ?>
                              myOrdinatiAppoggioJS.push(<?php echo json_encode($value); ?>)
                              <?php
                            } ?>


                            <?php foreach ($myOrdinatiProdottiPhp as $value) {
                              ?>
                              myOrdinatiProdottiJS.push(<?php echo json_encode($value); ?>)
                              <?php
                            } ?>


                            <?php foreach ($myOrdinateQuantitaPhp as $value) {
                              ?>
                              myOrdinateQuantitaJS.push(<?php echo json_encode($value); ?>)
                              <?php
                            } ?>


                            <?php foreach ($myOrdinatiPrezziPhp as $value) {
                              ?>
                              myOrdinatiPrezziJS.push(<?php echo json_encode($value); ?>)
                              <?php
                            } ?>


                            console.log("multi-vettore ordinati: ", myOrdinatiAppoggioJS);
                            console.log("prodotti ordinati: ", myOrdinatiProdottiJS);
                            console.log("prezzi ordinati: ", myOrdinatiPrezziJS);
                            console.log("quantita ordinati: ", myOrdinateQuantitaJS);
                          </script>

                        <?php }

                        for ($np=0; $np<count($myTuttiProdottiPhp) ;$np++){ /*np = numero prodotto, indica l'indice del prodotto corrente*/
                          ?>
                          <li class="my-contenitore-line-riepilogo my-<?php echo str_replace(' ', '', $myTuttiProdottiPhp[$np]); ?>-toggle-li">
                            <button class="btn btn-default my-diminuisci-quantita" onclick="decreaseTotal(<?php echo $np; ?>)">-</button>
                            <div class="my-specifico-alimento">
                              <?php
                              /*controllo per la quantita*/
                              if (in_array(json_encode($myTuttiProdottiPhp[$np]), $myOrdinatiProdottiPhp)){
                                for ($npo=0; $npo<count($myOrdinatiProdottiPhp) ;$npo++){
                                  if (json_encode($myTuttiProdottiPhp[$np]) == json_encode($myOrdinatiProdottiPhp[$npo])){ /*npo = numero prodotto ordinato, indica l'indice del prodotto ordinato corrente*/
                                    ?>
                                      <div class="my-quantita-alimento-ordinate my-quantita-<?php echo str_replace(' ', '', $myTuttiProdottiPhp[$np]); ?>"> <?php echo $myOrdinateQuantitaPhp[$npo]?></div>
                                      <script type="text/javascript">
                                        myTutteQuantitaJS[<?php echo $np; ?>] = myOrdinateQuantitaJS[<?php echo $npo; ?>];
                                      </script>
                                    <?php
                                  }
                                }
                              }
                              else
                              {
                                ?>
                                  <div class="my-quantita-alimento-ordinate my-quantita-<?php echo str_replace(' ', '', $myTuttiProdottiPhp[$np]); ?>"> <?php echo $myTutteQuantitaPhp[$np] ?></div>
                                <?php
                              }
                               ?>
                              <div> x </div>
                              <div class="my-nome-specifico-alimento"><?php echo $myTuttiProdottiPhp[$np]; ?></div>
                            </div>
                            <div class="my-prezzo-alimento">
                              <script type="text/javascript">
                                console.log("Indice NPPPPPPPPPPPPPPPPPPPPPPPP: " + '<?php echo $np ?>');
                              </script>
                              <?php
                            if(($myTuttiScontiPhp[$np]*1) > 0) {
                              echo number_format((float)$myTuttiScontiPhp[$np]*1, 2, '.', '') . " €";
                            } else {
                              echo $myTuttiPrezziPhp[$np] . " €";
                            }
                            ?></div>
                          </li>

                          <?php
                        }

                  } else {
                    for ($np=0; $np<count($myTuttiProdottiPhp) ;$np++){
                        ?>
                        <li class="my-contenitore-line-riepilogo my-<?php echo str_replace(' ', '', $myTuttiProdottiPhp[$np]); ?>-toggle-li">
                          <button class="btn btn-default my-diminuisci-quantita" onclick="decreaseTotal(<?php echo $np; ?>)">-</button>
                          <div class="my-specifico-alimento">
                            <div class="my-quantita-alimento-ordinate my-quantita-<?php echo str_replace(' ', '', $myTuttiProdottiPhp[$np]); ?>"> <?php echo $myTutteQuantitaPhp[$np] ?></div>
                            <div> x </div>
                            <div class="my-nome-specifico-alimento"><?php echo $myTuttiProdottiPhp[$np]; ?></div>
                          </div>
                        <div class="my-prezzo-alimento"><?php
                        if($myTuttiScontiPhp[$np] > 0) {
                          echo $myTuttiScontiPhp[$np] . " €";
                        } else {
                          echo $myTuttiPrezziPhp[$np] . " €";
                        }
                        ?> </div>
                      </li>
                      <?php
                    }
                   }
                } ?>


          </ul>



        </aside>
      <?php }?>

      </div>

                <div id="ac-id-contenitore-listino" class="my-contenitore-listino-cliente">


                  <div class="panel-group big-padding" id="accordion" role="tablist" aria-multiselectable="true">

                    <div id="my-fase2-ul-style" class="panel panel-default">
                    <?php


                      // DB connection
                      include_once 'utils/db_connect.php';
                      // Internal data query, no danger of SQL injection
                      $mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
                      $result = mysqli_query($mysqli, "SELECT `name` FROM `categories`");
                      $mysqli->close();
                      if ($result->num_rows > 0) {
                          $categories = array();
                          while ($row = $result->fetch_assoc()) {
                              array_push($categories, $row);
                          }
                          for ($i = 0; $i < count($categories); $i++) {
                              ?>

                              <div id="heading<?php echo $categories[$i]['name'];?>">
                                <h4>
                                  <a class="my-fase2-accordion-line" role="button" id="<?php echo $categories[$i]['name'];?>-row" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $categories[$i]['name'];?>" aria-expanded="true" aria-controls="collapse<?php echo $categories[$i]['name'];?>">
                                    <?php echo $categories[$i]['name'];?>
                                  </a>

                                </h4>
                              </div>

                              <ul id="collapse<?php echo $categories[$i]['name'];?>" class="panel-collapse  my-fase2-container-line-food collapse"  aria-labelledby="heading<?php echo $categories[$i]['name'];?>">
                                <li  class="my-contenitore-line-accordion line-description">
                                  <div class="my-nome-specifico-alimento-accordion">Nome</div>
                                  <div class="my-contenitore-prezzoSconto-alimento-accordion">
                                    <div class="my-prezzo-alimento-accordion">Prezzo</div>
                                    <div class="my-sconto-alimento-accordion">Sconto</div>
                                  </div>
                                  <div class="colonna-azione">Azione</div>
                                </li>

                                <li class="my-lischifo"> <hr class="my-divi-f2">  </li>
                                <?php
                                    $mysqli1 = new mysqli(HOST, USER, PASSWORD, DATABASE);

                                    // Internal data query, no danger of SQL injection
                                    $result1 = mysqli_query($mysqli1, "SELECT * FROM `products` WHERE `enabled` = 1 AND `product_id` IN (SELECT `product_id` FROM `prod_cat` WHERE `category_id` IN (SELECT `category_id` FROM `categories` WHERE `name` = '" . $categories[$i]['name'] . "'))");
                                    if ($result1->num_rows > 0) {
                                        $products = array();
                                        while ($row = $result1->fetch_assoc()) {
                                            array_push($products, $row);
                                        }
                                    }

                                    foreach ($products as $prod) {
                                  ?>
                                          <li id="<?php echo str_replace(' ', '', $prod['name']);?>" class="my-contenitore-line-accordion">
                                            <div class="my-nome-specifico-alimento-accordion"><?php echo $prod['name'];?></div>
                                            <div class="my-contenitore-prezzoSconto-alimento-accordion">
                                              <?php
                                                if($prod['sale'] > 0) {
                                                  ?>
                                                  <div class="my-prezzo-alimento-accordion"><del><?php echo $prod['price'] . " €"; ?></del></div>
                                                  <div class="my-sconto-alimento-accordion my-fase2-figa-roscia"><strong><?php echo $prod['sale'] . " €"; ?></strong></div>
                                                  <?php
                                                } else {
                                                  ?>
                                                  <div class="my-prezzo-alimento-accordion"><?php echo $prod['price'] . " €"; ?></div>
                                                  <div class="my-sconto-alimento-accordion my-fase2-figa-roscia"></div>
                                                  <?php
                                                }
                                               ?>
                                            </div>

                                            <?php if(!isset($_SESSION['admin']) || $_SESSION['admin'] == 0) {?>
                                            <button class="btn btn-default my-aumenta-quantita my-aumenta-quantita-<?php echo str_replace(' ', '', $prod['name']); ?>" <?php if(isset($_SESSION['admin']) && $_SESSION['admin'] == 0) {
                                            ?>onclick="increaseTotal(
                                              <?php
                                                if(isset($_SESSION['admin'])){
                                                for ($indnp = 0; $indnp < count($myTuttiProdottiPhp); $indnp++) {
                                                  if ($myTuttiProdottiPhp[$indnp] == $prod['name']){
                                                    $tempIndexTotale = $indnp;
                                                  }
                                                }
                                                if ($tempIndexTotale >= 0){
                                                  echo $tempIndexTotale;
                                                }else{
                                                  echo -1;
                                                }
                                              } else {
                                                echo 0;
                                              }
                                              ?>
                                              )"<?php } else {?>
                                                onclick="window.location='login.php';"
                                              <?php } ?>>+</button>
                                            <?php } else {?>


                                              <script type="text/javascript">
                                                var currentAction = "delete";
                                                var currentAmount = 0.00;

                                                function resetVarAndHideForm(nomePiatto){
                                                  currentAction = "delete";
                                                  currentAmount = 0.00;
                                                  hideIndicaPrezzoLine();
                                                  $( '.my-fire-click-'+nomePiatto.replace(/ /g, "") ).click ();
                                                }

                                                function hideIndicaPrezzoLine(){
                                                  $(".my-fase2-hide-indicaPrezzoLine").hide();
                                                }

                                                function showIndicaPrezzoLine(){
                                                  $(".my-fase2-hide-indicaPrezzoLine").show();
                                                }

                                                function setActionSelection(mySelect){
                                                  currentAction = $(mySelect).val();
                                                  if(currentAction=="delete" || currentAction=="r_sale"){
                                                    hideIndicaPrezzoLine();
                                                  } else{
                                                    showIndicaPrezzoLine();

                                                  }
                                                }

                                                function changePriceModificaPiatto(input){
                                                  currentAmount = $(input).val();
                                                }

                                                function doOnDelete(nomePiatto) {
                                                  console.log("Provo ad eliminare " + nomePiatto);
                                                  var response = $.post("admin/manageProducts.php", {
                                                      nomePiatto: nomePiatto,
                                                      action: "delete"
                                                  }, "json");
                                                  response.success(function(data) {
                                                    var response = jQuery.parseJSON(data);
                                                    if(response.status === "success") {
                                                      $('#' + nomePiatto.replace(/ /g, "")).hide();
                                                    } else if(response.status === "error") {
                                                      alert(response.data);
                                                    }
                                                  });
                                                  response.done(function(data) {
                                                    resetVarAndHideForm(nomePiatto);
                                                  });
                                                }

                                                function doOnSet(nomePiatto) {
                                                  console.log("Provo ad eliminare " + nomePiatto);
                                                  var response = $.post("admin/manageProducts.php", {
                                                      nomePiatto: nomePiatto,
                                                      action: "set",
                                                      nuovoPrezzo: currentAmount
                                                  }, "json");
                                                  response.success(function(data) {
                                                    var response = jQuery.parseJSON(data);
                                                    if(response.status === "success") {
                                                    $('#' + nomePiatto.replace(/ /g, "")).find('.my-prezzo-alimento-accordion').text(currentAmount+"€");
                                                    } else if(response.status === "error") {
                                                      alert(response.data);
                                                    }
                                                  });
                                                  response.done(function(data) {
                                                    resetVarAndHideForm(nomePiatto);
                                                  });
                                                }

                                                function doOnRSale(nomePiatto) {
                                                  console.log("Provo ad eliminare " + nomePiatto);
                                                  var response = $.post("admin/manageProducts.php", {
                                                      nomePiatto: nomePiatto,
                                                      action: "r_sale"
                                                  }, "json");
                                                  response.success(function(data) {
                                                    var response = jQuery.parseJSON(data);
                                                    if(response.status === "success") {
                                                      $('#' + nomePiatto.replace(/ /g, "")).find('.my-prezzo-alimento-accordion').html($('#' + nomePiatto.replace(/ /g, "")).find('.my-prezzo-alimento-accordion').text());
                                                      $('#' + nomePiatto.replace(/ /g, "")).find('.my-sconto-alimento-accordion').html('');
                                                    } else if(response.status === "error") {
                                                      alert(response.data);
                                                    }
                                                  });
                                                  response.done(function(data) {
                                                    resetVarAndHideForm(nomePiatto);
                                                  });
                                                }

                                                function doOnSale(nomePiatto) {
                                                  console.log("Provo ad eliminare " + nomePiatto);
                                                  var response = $.post("admin/manageProducts.php", {
                                                      nomePiatto: nomePiatto,
                                                      action: "sale",
                                                      prezzoScontato: currentAmount
                                                  }, "json");
                                                  response.success(function(data) {
                                                    var response = jQuery.parseJSON(data);
                                                    if(response.status === "success") {
                                                      $('#' + nomePiatto.replace(/ /g, "")).find('.my-prezzo-alimento-accordion').html('<del>' + $('#' + nomePiatto.replace(/ /g, "")).find('.my-prezzo-alimento-accordion').text() + '</del>');
                                                      $('#' + nomePiatto.replace(/ /g, "")).find('.my-sconto-alimento-accordion').html('<strong>' + currentAmount + ' €</strong>');
                                                    } else if(response.status === "error") {
                                                      alert(response.data);
                                                    }
                                                  });
                                                  response.done(function(data) {
                                                    resetVarAndHideForm(nomePiatto);
                                                  });
                                                }

                                                function mystampa(nome){
                                                  console.log(nome);
                                                }


                                                function doOnApplica(nomePiatto){
                                                  switch (currentAction) {
                                                    case "delete":
                                                      console.log("nessun amount delete");
                                                      doOnDelete(nomePiatto);
                                                      break;
                                                    case "r_sale":
                                                      console.log("nessun amount r_sale");
                                                      doOnRSale(nomePiatto);
                                                      break;
                                                    case "sale":
                                                      console.log(currentAmount + " sale");
                                                      doOnSale(nomePiatto);
                                                      break;
                                                    case "set":
                                                      console.log(currentAmount + " set");
                                                      doOnSet(nomePiatto);
                                                      break;

                                                    default:
                                                      console.log("default");
                                                  }

                                                }

                                                $(document).ready(function() {
                                                  hideIndicaPrezzoLine();
                                                });


                                              </script>




                                              <a class="btn btn-primary my-aggiungi-nuovo-sconto my-fire-click-<?php echo str_replace(' ', '', $prod['name']); ?>" data-placement="left" data-popover-content="#id-imposta-offerta-<?php echo str_replace(' ', '', $prod['name']); ?>" data-toggle="popover" href="#" tabindex="0">Modifica piatto</a>

                                              <div class="hidden center" id="id-imposta-offerta-<?php echo str_replace(' ', '', $prod['name']); ?>">
                                                  <form class="popover-body my-contenitore-pop-sconto my-popover-body-modifica" action="/#">

                                                    <div class="my-contenitore-line-popover">
                                                      <label for="my-azione-form-agg" class="my-label-nuovo-prezzo my-label-pop-sconto my-elemento-left">Azione:</label>
                                                      <!--input id="my-prezzo-offerta-form-agg" class="my-form-pop-sconto my-blanket-nuovo-prezzo" type="text" name="my-plate-form" placeholder="prezzo in €"-->
                                                      <select id="my-azione-form-agg" name="azione" class="my-form-pop-sconto my-elemento-right"  onchange="setActionSelection(this)">
                                                        <option value="delete">Rimuovi piatto</option>
                                                        <option value="sale">Aggiungi sconto</option>
                                                        <option value="set">Cambia prezzo</option>
                                                        <option value="r_sale">Rimuovi sconto</option>
                                                      </select>
                                                    </div>



                                                    <div class="my-contenitore-line-popover my-fase2-hide-indicaPrezzoLine">
                                                      <label for="my-prezzo-modifica-form-agg" class="my-label-tempo my-label-pop-sconto my-elemento-left ">Indica prezzo:</label>
                                                      <input id="my-prezzo-modifica-form-agg" class="my-form-pop-sconto my-blanket-prezzo my-elemento-right  my-classe-form-modifica-piatto-prezzo-<?php echo str_replace(' ', '', $prod['name']); ?>" type="text" name="my-price-form" onchange="changePriceModificaPiatto(this)">
                                                    </div>



                                                    <div class="my-last-line-pop-sconto">
                                                      <input class="my-form-popover-modifica-pulsante " type="button" value="Applica" onclick="doOnApplica('<?php echo $prod['name']; ?>')">
                                                    </div>

                                                  </form>
                                              </div>

                                              <?php } ?>
                                          </li>

                                  <?php
                                }

                                if (isset($_SESSION['admin']) && $_SESSION['admin']==1){
                                  ?>

                                  <script type="text/javascript">






                                    function aggiungiPiatto(nomeCategoria ){

                                      var myNomeNuovoPiatto = $(".my-classe-form-aggiungi-nome-".concat(nomeCategoria)).val();

                                      var myPrezzoNuovoPiatto = $(".my-classe-form-aggiungi-prezzo-".concat(nomeCategoria)).val();

                                      console.log("aggiungi nuovo ", nomeCategoria, ": ", myNomeNuovoPiatto, " ", myPrezzoNuovoPiatto);

                                      var response = $.post("admin/manageProducts.php", {
                                          nomePiatto: myNomeNuovoPiatto,
                                          prezzoPiatto: myPrezzoNuovoPiatto,
                                          categoriaPiatto: nomeCategoria,
                                          action: "add"
                                      }, "json");
                                      response.success(function(data) {
                                        var response = jQuery.parseJSON(data);
                                        console.log("Richiesta effettuata con successo!");
                                        if(response.status === "success") {
                                          $("#collapse" + nomeCategoria.replace(/ /g, "")).find('#btn-aggiungi' + nomeCategoria.replace(/ /g, "")).before(
                                            '<li id = ' + myNomeNuovoPiatto.replace(/ /g, "") + ' class="my-contenitore-line-accordion"> ' +
                                              '<div class="my-nome-specifico-alimento-accordion">' + myNomeNuovoPiatto + '</div>' +
                                              '<div class="my-contenitore-prezzoSconto-alimento-accordion">' +
                                              '<div class="my-prezzo-alimento-accordion">' + myPrezzoNuovoPiatto +  '€</div>' +
                                              '<div class="my-sconto-alimento-accordion"></div></div>' +
                                              '<a class="btn btn-primary my-aggiungi-nuovo-sconto my-fire-click-' + myNomeNuovoPiatto.replace(/ /g, "") + '" data-placement="left" data-popover-content="#id-imposta-offerta-' + myNomeNuovoPiatto.replace(/ /g, "") + '" data-toggle="popover" href="#" tabindex="0">Modifica piatto</a>' +
                                                '<div class="hidden center" id="id-imposta-offerta-' + myNomeNuovoPiatto.replace(/ /g, "") + '">' +
                                                  '<form class="popover-body my-contenitore-pop-sconto my-popover-body-modifica" action="/#">' +
                                                      '<div class="my-contenitore-line-popover">' +
                                                        '<label for="my-azione-form-agg" class="my-label-nuovo-prezzo my-label-pop-sconto my-elemento-left">Azione:</label>' +
                                                        '<select id="my-azione-form-agg" name="azione" class="my-form-pop-sconto my-elemento-right" onchange="setActionSelection(this)">' +
                                                        '<option value="delete">Rimuovi piatto</option>' +
                                                        '<option value="sale">Aggiungi sconto</option>' +
                                                        '<option value="set">Cambia prezzo</option>' +
                                                        '<option value="r_sale">Rimuovi sconto</option></select></div>' +
                                                      '<div class="my-contenitore-line-popover my-fase2-hide-indicaPrezzoLine">' +
                                                        '<label for="my-prezzo-modifica-form-agg" class="my-label-tempo my-label-pop-sconto my-elemento-left">Indica prezzo:</label>' +
                                                        '<input id="my-prezzo-modifica-form-agg" class="my-form-pop-sconto my-blanket-prezzo my-elemento-right my-fase2-hide-indicaPrezzoLine my-classe-form-modifica-piatto-prezzo-' + myNomeNuovoPiatto.replace(/ /g, "") + '" type="text" name="my-price-form" onchange="changePriceModificaPiatto(this)">' +
                                                      '</div>' +
                                                      '<div class="my-last-line-pop-sconto">' +
                                                        '<input class="my-form-popover-modifica-pulsante " type="button" value="Applica" onclick="doOnApplica(\'' +myNomeNuovoPiatto.replace(/ /g, "") + '\')"></div></form></div></li>');
                                                        $("#" + myNomeNuovoPiatto.replace(/ /g, "")).hide().fadeIn(2000);
                                                        hideIndicaPrezzoLine();
                                                        $("#" + myNomeNuovoPiatto.replace(/ /g, "")).find('.my-aggiungi-nuovo-sconto').popover({
                                                            html : true,
                                                            content: function() {
                                                              var content = $(this).attr("data-popover-content");
                                                              return $(content).children(".popover-body").html();
                                                            },
                                                            title: function() {
                                                              var title = $(this).attr("data-popover-content");
                                                              return $(title).children(".popover-heading").html();
                                                            }
                                                        });
                                        } else if(response.status === "error"){
                                          alert(response.data);
                                        }
                                      });
                                      response.done(function() {
                                        $( '.my-aggiungi-npiatto-'+nomeCategoria.replace(/ /g, "") ).click ();
                                      });
                                      }



                                  </script>

                                  <div id="btn-aggiungi<?php echo $categories[$i]['name'];?>" class="my-contenitore-line-accordion">
                                    <a class="btn btn-primary my-aggiungi-nuovo-piatto my-aggiungi-npiatto-<?php echo $categories[$i]['name'];?>" data-placement="top" data-popover-content="#id-popover-<?php echo $categories[$i]['name'];?>" data-toggle="popover" href="#" tabindex="0">Aggiungi piatto</a>
                                  </div>

                                  <div class="hidden center" id="id-popover-<?php echo $categories[$i]['name'];?>">
                                    <form class="popover-body" action="/#">
                                      <div class="my-contenitore-line-popover-name my-contenitore-line-popover">
                                        <label for="my-nome-pizza-form-agg" class="my-label-form-popover">Nome piatto:</label>
                                        <input id="my-nome-pizza-form-agg my-id-form-aggiungi-nome-<?php echo $categories[$i]['name'];?>" class="my-form-popover my-classe-form-aggiungi-nome-<?php echo $categories[$i]['name'];?>" type="text" name="my-plate-form" placeholder="nome">
                                      </div>
                                      <div class="my-contenitore-line-popover-prezzo my-contenitore-line-popover">
                                        <label for="my-prezzo-pizza-form-agg" class="my-label-form-popover">Prezzo:</label>
                                        <input id="my-prezzo-pizza-form-agg my-id-form-aggiungi-prezzo-<?php echo $categories[$i]['name'];?>" class="my-form-popover my-classe-form-aggiungi-prezzo-<?php echo $categories[$i]['name'];?>" type="text" name="lastname" placeholder="0.00 €">
                                      </div>

                                      <div class="my-contenitore-line-popover-pulsante my-contenitore-last-line-popover">
                                        <input class="my-form-popover-pulsante" type="button" value="Aggiungi piatto" onclick="aggiungiPiatto('<?php echo $categories[$i]['name']; ?>')">
                                      </div>
                                    </form>
                                  </div>

                                  <?php
                                }
                                  $mysqli1->close();
                                  ?>

                              </ul>
                              <?php
                          }
                      }?>

            </div>



          </div> <!--accordion-->


        </div> <!--terza parte-->

      </div>



    </div>
  </div>

<!--navbar in fondo-->
<?php if (!isset($_SESSION['admin']) || $_SESSION['admin']==0){ ?>
  <nav class="navbar navbar-default navbar-fixed-bottom my-bottom-navbar-fase3">
    <div class="container-fluid miaNav">
      <div class="mio-contenitore-nav-bottom">
        <div class="numero-elementi-nel-carrello elem-bottom-nav"></div>

        <div class="totale-conto-carrello elem-bottom-nav"></div>

        <div class="vai-al-pagamento elem-bottom-nav"><a id="my-fase-2-btn-to-cart" class="btn btn-default my-fase-2-btn-to-cart">Ordina e paga</a></div>
      </div>
    </div>
  </nav>
<?php } ?>

  <!-- Writes correct label inside navbar button -->
      <?php
      /*
      echo $_SESSION["user_id"];
      echo $_SESSION["name"];
      echo $_SESSION["surname"];
      echo $_SESSION["admin"];
      */
          if(empty($_SESSION["user_id"]) && empty($_SESSION["name"]) && empty($_SESSION["surname"])) { ?>  <!-- No login yet -->
              <script>
                  $(".my-elemento-client").attr("style", "display: none !important;");
                  $(".my-elemento-unlogged").attr("style", "display: block !important;");
                  $("#my-fase2-cart-id").attr("href", "login.php");
                  $("#my-fase-2-btn-to-cart").attr("href", "login.php");
                  $(".my-fase2-to-index").attr("href", "index.php");
                  $(".my-sidebar-aside-container").attr("style", "display: none !important;");
                  console.log("non loggato");



                  $(document).ready(function() {
                    $(".my-aumenta-quantita").click(function() {});
                    $(".my-aumenta-quantita").attr("href", "login.php");
                    //$(".my-aumenta-quantita").hide();
                    $("#ac-id-contenitore-listino").removeClass("my-contenitore-listino-cliente");
                    $("#ac-id-contenitore-listino").addClass("my-contenitore-listino-venditore");

                  });

              </script>
          <?php
        } else { // Login
            if ($_SESSION['admin']) { ?>
              <script>
              $(".my-elemento-client").attr("style", "display: none !important;");
              $(".my-elemento-admin").attr("style", "display: block !important;");
              $(".my-elemento-unlogged").attr("style", "display: none !important;");
              $("#my-fase2-cart-id").attr("style", "display: none !important;"); /*ocio può dar danno*/
              $(".navbar-fixed-bottom").attr("style", "display: none !important;");
              $(".my-sidebar-aside-container").attr("style", "display: none !important;");
              $(".my-fase2-to-ordiniPage").attr("href", "ordiniPage.php");
              $(".my-fase2-to-index").attr("href", "index.php");
              $(".my-fase2-to-logout").attr("href", "utils/logout.php");
              console.log("admin");


              $(document).ready(function() {
                $("#ac-id-contenitore-listino").removeClass("my-contenitore-listino-cliente");
                $("#ac-id-contenitore-listino").addClass("my-contenitore-listino-venditore");



              });

              </script> <?php
            } else { ?>
              <script>
              $(".my-elemento-client").attr("style", "display: block !important;");
              $(".my-elemento-unlogged").attr("style", "display: none !important;");
              /*
              $("#my-fase2-cart-id").attr("href", "cart.php");
              $("#my-fase-2-btn-to-cart").attr("href", "cart.php");
              */

              console.log("utente");
              </script> <?php
            }
          }
      ?>



    <script type="text/javascript">
      var nomeProdotto = "0"; //usato nella ricerca dell'indice
      var totalToPay = 0.00;
      var nArtTot = 0;

      function calcolanArtTot(){
        var nArticoliIni = 0;

        for (i = 0; i < myTuttiPrezziJS.length; i++) {
          if (myTutteQuantitaJS[i]>0){
            nArticoliIni+= 1*myTutteQuantitaJS[i]; /*è una porcheria ma se no leggerebbe la quantità come stringa e farebbe un concat dei numeri*/
          }
        }

        nArtTot = nArticoliIni;
      }

      function calcolaTotale(){
        var totIni= 0.00;

        for (i = 0; i < myTuttiPrezziJS.length; i++) {
          if( myTutteQuantitaJS[i]!=0){
            if (myTuttiScontiJS[i]>0){
              totIni += myTuttiScontiJS[i] * myTutteQuantitaJS[i];
              console.log("uso lo scontooooo");
            }else{
              totIni += myTuttiPrezziJS[i] * myTutteQuantitaJS[i];
              console.log("SCONTO indice cibo", i, " tutti sconti: ", myTuttiScontiJS[i]);
            }
          }
        }

        totalToPay = totIni;
        totalToPay = totalToPay.toFixed(2);
      }

      function checkName(foodName) {
        return foodName == nomeProdotto ;
      }

      function indiceInTuttiProdottiDi(nomeP) {
        nomeProdotto = nomeP;
        return myTuttiProdottiJS.findIndex(checkName);
      }

      function indiceInToPostProdottiDi(nomeP) {
        nomeProdotto = nomeP;
        return myToPostProdottiJS.findIndex(checkName);
      }

      function diminuisciProdottoToPost(pName){

        var indexToPost = indiceInToPostProdottiDi(pName);
        if (indexToPost < 0){
          console.log("errore nel diminuire quantità, ", pName, " non trovato in myToPostProdottiJS; myToPostProdottiJS= ", myToPostProdottiJS);
        } else {
          if ((myToPostQuantitaJS[indexToPost]*1) == 1){
            myToPostQuantitaJS.splice(indexToPost, 1);
            myToPostProdottiJS.splice(indexToPost, 1);
          } else {
            myToPostQuantitaJS[indexToPost] = ((myToPostQuantitaJS[indexToPost]*1)-1);
          }
        }
      }

      function aumentaProdottoToPost(pName){
        var indexToPost = indiceInToPostProdottiDi(pName);
        if (indexToPost < 0){
          myToPostProdottiJS.push(pName);
          myToPostQuantitaJS.push("1");
        } else {
            myToPostQuantitaJS[indexToPost] = ((myToPostQuantitaJS[indexToPost]*1)+1);
        }
      }

      function refreshTotalAndnArticles(){
        var totIni= 0.00;
        var nArticoliIni = 0;

        for (i = 0; i < myTuttiPrezziJS.length; i++) {
          if( myTutteQuantitaJS[i]!=0){

            if (myTuttiScontiJS[i]>0){
              totIni += myTuttiScontiJS[i] * myTutteQuantitaJS[i];
              console.log("uso lo scontooooo");
            }else{
              totIni += myTuttiPrezziJS[i] * myTutteQuantitaJS[i];
              console.log("SCONTO indice cibo", i, " tutti sconti: ", myTuttiScontiJS[i]);
            }

            if (myTutteQuantitaJS[i]>0){
              nArticoliIni+= 1*myTutteQuantitaJS[i]; /*è una porcheria ma se no leggerebbe la quantità come stringa e farebbe un concat dei numeri*/
            }
          }
        }

        console.log("tot parziale:");
        console.log(totIni);
        console.log("nArticoli parziale:");
        console.log(nArticoliIni);

        document.getElementsByClassName("numero-elementi-nel-carrello")[0].innerHTML = nArticoliIni + " articoli";
        document.getElementsByClassName("totale-conto-carrello")[0].innerHTML = "Totale: " + totIni.toFixed(2) + " €";
      }

      function correggiArrayTutteQuantita(){

        for (var j=0; j<myOrdinatiProdottiJS.length; j++){
          for (var i=0; i<myTuttiProdottiJS.length; i++){
            if (myTuttiProdottiJS[i].replace(/ /g,'') == myOrdinatiProdottiJS[j].replace(/ /g,'') ){
              console.log("devono essere uguali: ", myTuttiProdottiJS[i], " = ", myOrdinatiProdottiJS[j]);
              myTutteQuantitaJS[i] = "".concat(1*myOrdinateQuantitaJS[j]); //sono tutte stringhe almeno nel vettore a scanso di equivoci
            }
          }
        }

        console.log("vettore quantità a fine correzione: ", myTutteQuantitaJS);
      }

      function correggiQuantita(indice){
        $(".my-quantita-".concat(myTuttiProdottiJS[indice].replace(/ /g,''))).html(myTutteQuantitaJS[indice]);
      }

      function settaElementoCarrelloInvisibile(indice){
        $(".my-".concat(myTuttiProdottiJS[indice].replace(/ /g,'').concat("-toggle-li"))).hide();
        //console.log("prodotto ", myTuttiProdottiJS[indice], " , indice:  ", indice);
      }

      function settaElementoCarrelloVisibile(indice){
        $(".my-".concat(myTuttiProdottiJS[indice].replace(/ /g,'').concat("-toggle-li"))).show();
      }

      function correggiTutteQuantita(){
        for (var i=0; i<myTutteQuantitaJS.length; i++){
          if ((1*myTutteQuantitaJS[i])!=0){
            correggiQuantita(i);
            console.log("pos da correggere ", i);
            myToPostProdottiJS.push(myTuttiProdottiJS[i]);
            myToPostQuantitaJS.push(myTutteQuantitaJS[i]);
          } else {
            settaElementoCarrelloInvisibile(i);
          }
        }
      }

      function preparaPaginaOneCall(){
        correggiArrayTutteQuantita();
        correggiTutteQuantita();
      }

      function decreaseTotal(index) {

        myTutteQuantitaJS[index]--;
        console.log(myTutteQuantitaJS[index]);

        correggiQuantita(index);

        if ( myTutteQuantitaJS[index] == 0){
            settaElementoCarrelloInvisibile(index);
        }

        diminuisciProdottoToPost(myTuttiProdottiJS[index]);

        console.log("ecco ToPost", myToPostProdottiJS);
        console.log(myToPostQuantitaJS);

        refreshTotalAndnArticles();
      }

      function increaseTotal(index) {
        myTutteQuantitaJS[index]++;
        console.log(myTutteQuantitaJS[index]);

        correggiQuantita(index);

        if ( myTutteQuantitaJS[index] == 1){
            settaElementoCarrelloVisibile(index);
        }

        aumentaProdottoToPost(myTuttiProdottiJS[index]);

        console.log("ecco ToPost", myToPostProdottiJS);
        console.log(myToPostQuantitaJS);


        refreshTotalAndnArticles();
      }

      function salvaInfo(myaddress){
        calcolaTotale();
        calcolanArtTot();
        console.log("salverò totale= ", totalToPay, " nArticoli= ", nArtTot);
        $.post("cartHandlePhp/set-values-to-paid.php", {
            total: totalToPay,
            nArticoli: nArtTot,
            quantities: myTutteQuantitaJS.join(),
            products: myTuttiProdottiJS.join(),
            prices: myTuttiPrezziJS.join()
        }).done(function(e) {
              window.location.href = "".concat(myaddress);
        });
      }

      $(document).ready(function() {
        <?php
          if(isset($_SESSION['admin']) && $_SESSION['admin'] == 0) {
        ?>
        preparaPaginaOneCall();
        refreshTotalAndnArticles();
        console.log("prodotti to post", myToPostProdottiJS);
        console.log("quantita to post", myToPostQuantitaJS);
        <?php
          }
        ?>
      });
    </script>

    <script type="text/javascript">
    var myClass;
    var dir ="";
      $(document).ready(function() {

        <?php if(!empty($_SESSION["user_id"]) && !empty($_SESSION["name"]) && !empty($_SESSION["surname"])) { ?>

        $(".my-fase-2-btn-to-cart, .my-fase2-to-accountPage, .my-fase2-to-ordiniPage, .my-fase2-to-logout, .my-fase2-to-index").click(function() {
          calcolaTotale();
          calcolanArtTot();
          console.log("salverò totale= ", totalToPay, " nArticoli= ", nArtTot);
          myClass = $(this).attr("class");
          console.log("ORDINA E PAGA CLASSE: " + myClass);
          switch (myClass) {
            case "btn btn-default my-fase-2-btn-to-cart":
            console.log("HO PREMUTO QUEL MALEDETTO PULSANTEEE");
              dir = "cart.php";
              break;
            case "my-fase2-to-accountPage":
            console.log(" E CHENNESSO 1");
              dir = "accountPage.php";
              break;
            case "my-fase-2-btn-to-cart":
            console.log(" E CHENNESSO 2");
              dir = "cart.php";
              break;
            case "my-fase2-to-ordiniPage":
            console.log(" E CHENNESSO 3");
              dir = "ordiniPage.php";
              break;


            case "my-fase2-to-logout":
            console.log(" E CHENNESSO 4");
              dir = "utils/logout.php";
              break;
            case "navbar-brand my-fase2-to-index":
            console.log(" E CHENNESSO 5");
              dir = "index.php";
              break;



            default:

          }
          $.post("cartHandlePhp/update-cart-db.php", {
              total: totalToPay,
              nArticoli: nArtTot,
              userId : <?php echo $user_id; ?>,
              quantities: myToPostQuantitaJS.join(),
              products: myToPostProdottiJS.join()
          }).done(function(e) {
            console.log("Myclass :" + myClass);
            console.log(dir);
              window.location.href = dir;
            });
        });

        <?php } ?>

      });



    </script>






  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>


  <script>
    $(function(){
        $("[data-toggle=popover]").popover({
            html : true,
            content: function() {
              var content = $(this).attr("data-popover-content");
              return $(content).children(".popover-body").html();
            },
            title: function() {
              var title = $(this).attr("data-popover-content");
              return $(title).children(".popover-heading").html();
            }
        });
    });

    $(document).ready(function() {
      <?php
        if(isset($_GET['categoria'])) {
          ?>
          $('#collapse' + '<?php echo $_GET['categoria'];?>').addClass('collapse in');
          $('#collapse' + '<?php echo $_GET['categoria'];?>').attr("aria-expanded", true);
          <?php
        }
      ?>
    });

  </script>
</div>
</body>
</html>
