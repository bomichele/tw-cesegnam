<!-- Secure session -->
<?php include_once 'utils/sec_session.php';
      include_once 'utils/db_connect.php';

    sec_session_start();
    if(!empty($_SESSION['user_id']) && !empty($_SESSION['name']) && !empty($_SESSION['surname'])) {
        $user_id = $_SESSION['user_id'];
	  }
    $orderid = $_SESSION['order_id'];
    $result = mysqli_query($mysqli, "SELECT * FROM `orders` WHERE `order_id` = " . $_SESSION['order_id']);
    $row = $result->fetch_assoc();
    $totale = $row['totale'];
    $oraCons = $row['oraConsegna'];
    $indirizzo = $row['delivery_address'];
    $stato = $row['state'];
    $pagamento = $row['payment'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <script src="jQuery/jquery-3.2.1.js"></script>
  <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="background.css">
  <link rel="stylesheet" href="successfulOrderStyle.css">
  <title>Ordine effettuato</title>
</head>
<body>

  <!-- navbar in alto-->
    <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid miaNav">
      <div class="mio-contenitore-nav">
        <a class="navbar-brand" href="index.php" >
          <img class="myTitoloNav" alt="Title" src="foto/Cesegnam.png">
        </a>
      </div>
    </div>
    </nav>


<!-- vero corpo-->

  <div class="my-corpo">

    <div class="my-contenitore">
      <h2>Ordine effettuato!</h2>

      <div class="my-quadretto">

        <div class="my-line-quadretto">
          <div class="my-label-idOrdine my-label-quadretto scritta-bianca">ID ordine:</div>
          <div class="my-val-idOrdine my-val-quadretto scritta-bianca"><?php echo $_SESSION['order_id'];?></div>
        </div>

        <div class="my-line-quadretto">
          <div class="my-label-idOrdine my-label-quadretto scritta-bianca">Cliente:</div>
          <div class="my-val-idOrdine my-val-quadretto scritta-bianca"><?php echo $_SESSION['name'] . " " . $_SESSION['surname']?></div>
        </div>

        <div class="my-line-quadretto">
          <div class="my-label-prezzo my-label-quadretto scritta-bianca">Prezzo pagato:</div>
          <div class="my-val-prezzo my-val-quadretto scritta-bianca"><?php echo number_format((float)$totale, 2, '.', '');?>€</div>
        </div>

        <div class="my-line-quadretto">
          <div class="my-label-ora my-label-quadretto scritta-bianca">Ora di consegna</div>
          <div class="my-val-ora my-val-quadretto scritta-bianca"><?php echo $oraCons;?></div>
        </div>

        <div class="my-line-quadretto">
          <div class="my-label-indirizzo my-label-quadretto scritta-bianca">Indirizzo:</div>
          <div class="my-val-indirizzo my-val-quadretto scritta-bianca"><?php echo $indirizzo;?></div>
        </div>

        <div class="my-line-quadretto">
          <div class="my-label-stato my-label-quadretto scritta-bianca">Stato:</div>
          <div class="my-val-stato my-val-quadretto scritta-bianca"><?php echo $stato;?></div>
        </div>

        <div class="my-line-quadretto">
          <div class="my-label-pagamento my-label-quadretto scritta-bianca">Metodo di pagamento:</div>
          <div class="my-val-pagamento my-val-quadretto scritta-bianca"><?php echo $pagamento;?></div>
        </div>

      </div>

    </div>




  </div>

  <?php
  unset($_SESSION['nArticoli']);
  unset($_SESSION['total']);
  unset($_SESSION['order_id']);
  $mailTO = $_SESSION['email'];
  $subject = "Cesegnam - Nuovo ordine";
  $message = $_SESSION['name'] . " " . $_SESSION['surname'] . " hai appena effettuato un ordine, controlla il pannello degli ordini.\nIndirizzo di consegna: " . $indirizzo . "\nOra di consegna: " . $oraCons;
  $headers = "From: cesegnam@gmail.com";
  if(!mail(utf8_decode($mailTO), utf8_decode($subject), utf8_decode($message), utf8_decode($headers)."\nContent-Type: text/plain; charset=UTF-8\nContent-Transfer-Encoding: 8bit\n")) {
    die("Error sending mail");
  }
  
  $mailTO = "cesegnam@gmail.com";
  $subject = "Cesegnam - Nuovo ordine";
  $message = "E' stato effettuato un nuovo ordine! Controlla il pannello degli ordini per maggiori dettagli.";
  $headers = "From: cesegnam@gmail.com";
    if(!mail(utf8_decode($mailTO), utf8_decode($subject), utf8_decode($message), utf8_decode($headers)."\nContent-Type: text/plain; charset=UTF-8\nContent-Transfer-Encoding: 8bit\n")) {
    die("Error sending mail");
  }
   ?>
  <script type="text/javascript">
  setTimeout("window.location='index.php'",5000);
  </script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>
