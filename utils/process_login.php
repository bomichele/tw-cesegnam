<?php
	include_once './db_connect.php';
	include_once './sec_session.php';
	include_once './functions.php';
	sec_session_start();
	if(isset($_POST['email'], $_POST['p'])) {
		   $email = $_POST['email'];
		   $password = $_POST['p'];
		   switch(login($email, $password, $mysqli)) {
				case -1:
					//Account disabilitato
					$mysqli->close();
					header("Location: ../login.php?error=-1");
					break;
				case 0:
					//Password non corretta
					$mysqli->close();
					header("Location: ../login.php?error=1");
					break;
				case 1:
					// Login eseguito
					$mysqli->close();
					header("Location: ../index.php");
					break;
				case 2:
					//Utente inesistente
					$mysqli->close();
					header("Location: ../login.php?error=2");
					break;
				case 3:
					//Login eseguito ma necessario il cambio password.
					$mysqli->close();
					header("Location: change_psw.php");
					break;
		   }
		   exit();
	} else {
		//Non sono state inviate le variabili corrette a questa pagina, dal metodo POST.
		$mysqli->close();
		header("Location: login.php?error=0");
		exit();
	}
?>
