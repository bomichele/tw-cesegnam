<?php
	include_once './db_connect.php';
	include_once './sec_session.php';
	include_once './functions.php';
	if(!empty($_POST['regName']) && !empty($_POST['regSurname']) && !empty($_POST['regEmail']) && !empty($_POST['p'])) {
		$name = $_POST['regName'];
		$surname = $_POST['regSurname'];
		$email = $_POST['regEmail'];
		$passwordBefore = $_POST['p'];
		$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
		$passwordAfter = hash('sha512', $passwordBefore.$random_salt);

		if ($insert_stmt = $mysqli->prepare("INSERT INTO users (name, surname, email, password, salt) VALUES (?, ?, ?, ?, ?)")) {
		   $insert_stmt->bind_param('sssss', $name, $surname, $email, $passwordAfter, $random_salt);
		   $result = $insert_stmt->execute();
		   if (!$result) {
				die('Invalid query: ' . mysql_error());
		   } else {
				sec_session_start();
				if(login($email, $passwordBefore, $mysqli) == true) {
					// Login eseguito
					$insert_stmt->close();
					$mysqli->close();
					header("Location: ../index.php");
					exit();
				} else {
					// Login fallito
					$insert_stmt->close();
					$mysqli->close();
					header("Location: ../login.php?error=1");
					exit();
				}
			}
		}
	} else {
		$mysqli->close();
		echo $_POST['regName'];
		echo $_POST['regSurname'];
		echo $_POST['regEmail'];
		echo $_POST['p'];
		/*header("Location: ../login.php?error=0"); */
		/*exit();*/
	}
?>
