<?php
  include_once 'db_connect.php';
  include_once 'sec_session.php';
 
  sec_session_start();

  if(isset($_POST['psw'])) {
    $passwordBefore = $_POST['psw'];
    $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
		$passwordAfter = hash('sha512', $passwordBefore.$random_salt);
    if ($stmt = $mysqli->prepare("UPDATE `users` SET `password`= ?, `salt`= ? WHERE `user_id` = ?")) {
                    $stmt->bind_param('ssi', $passwordAfter, $random_salt, $_SESSION['user_id']);
                    $stmt->execute();
                    $stmt->close();
    }
  } else {
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['surname'] = $_POST['surname'];
    $_SESSION['email'] = $_POST['email'];

    if ($stmt = $mysqli->prepare("UPDATE `users` SET `name`= ?, `surname`= ?, `email`= ? WHERE `user_id` = ?")) {
    echo $mysqli->error;
                    $stmt->bind_param('sssi', $_POST['name'], $_POST['surname'], $_POST['email'], $_SESSION['user_id']);
                    echo $mysqli->error;
                    $stmt->execute();
                    echo $mysqli->error;
                    $stmt->close();
    }
  }

echo "success";
?>
